#!/bin/env python

import json
from argparse import ArgumentParser
from collections import OrderedDict
from subprocess import Popen

import semver

tag_format = 'ljda-%d.%d.%d'


def bump(major, minor, patch, release=False, prerelease=None):
    with open('package.json') as pkgFile:
        package = json.load(pkgFile, object_pairs_hook=OrderedDict)

    package['version'] = semver.format_version(major, minor, patch, prerelease=prerelease)

    with open('package.json', 'w') as pkgFile:
        json.dump(package, pkgFile, indent=2, separators=(',', ': '))

    if release:
        print "Releasing version %s" % (package['version'])
        shell_execute("git commit -m \"chore(version): Release version %s\" package.json" % (package['version']))
    else:
        print "Bumping version to %s" % (package['version'])
        shell_execute("git commit -m \"chore(version): Bump version to %s\" package.json" % (package['version']))


def shell_execute(args):
    process = Popen(args, shell=True)
    process.wait()


def prepare():
    with open('package.json') as pkgFile:
        package = json.load(pkgFile, object_pairs_hook=OrderedDict)

    (major, _, _, minor, patch) = semver.parse(package['version']).values()
    (next_major, next_minor, next_patch) = (major, minor, patch)

    bump(major, minor, patch, release=True)

    print "Tagging " + tag_format % (major, minor, patch)
    shell_execute("git tag " + tag_format % (major, minor, patch))

    if patch == 0:
        next_minor += 1
    else:
        next_patch += 1

    bump(major, next_minor, next_patch, prerelease='SNAPSHOT')

    if patch == 0:
        print "Checking out " + tag_format % (major, minor, patch)
        shell_execute("git checkout " + tag_format % (major, minor, patch))

        print "Creating branch %d.%d" % (major, minor)
        shell_execute("git checkout -b %d.%d" % (major, minor))

        next_patch += 1
        bump(major, minor, next_patch, prerelease='SNAPSHOT')


def main():
    parser = ArgumentParser()
    commandParser = parser.add_subparsers(dest='command')

    prepareParser = commandParser.add_parser('prepare')
    prepareParser.set_defaults(func=prepare)

    args = parser.parse_args()

    args.func()

if __name__ == '__main__':
    main()

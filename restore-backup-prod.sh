#!/bin/bash -x

if [ -z "$1" ]; then
  echo "Usage: restore-backup.sh file"
  exit 1
fi

ROOT_PASSWD="ob7JLuD#QXi^bpNE"

KUBECONFIG=./kubeconfig.yml kubectl exec -n ljda-prod -it deploy/mysql -- mysql -p${ROOT_PASSWD} -e "DROP DATABASE aourell; CREATE SCHEMA aourell DEFAULT CHARACTER SET utf8 COLLATE utf8_bin;"

set -e

KUBECONFIG=./kubeconfig.yml kubectl exec -n ljda-prod -it deploy/mysql -- mysql -p${ROOT_PASSWD} aourell < "$1"

<?php

require_once 'abstract.php';

class JardinsAvalon_Shell_FinishingKnotOption extends Mage_Shell_Abstract {

  public function __construct() {
    parent::__construct();

    set_time_limit(0);
  }

  public function run() {
    $source_id = 475;

    echo "Loading source product with id $source_id\n";
    $source_product = Mage::getModel('catalog/product')->load($source_id);

    echo "Reading options\n";
    $product_opt = current($source_product->getOptions());

    $new_option = [
      'title' => $product_opt->getTitle(),
      'type' => $product_opt->getType(),
      'is_require' => $product_opt->getIsRequire(),
      'sort_order' => $product_opt->getSortOrder(),
      'values' => array_map(function ($option) {
        return [
          'title' => $option->getTitle(),
          'price' => $option->getPrice(),
          'price_type' => $option->getPriceType(),
          'sort_order' => $option->getSortOrder()
        ];
      }, $product_opt->getValues())
    ];

    echo "Looking for products to update\n";
    $col = Mage::getModel('catalog/product')->getCollection()
      ->addAttributeToSelect('*')
      ->addAttributeToFilter('type_id', ['eq' => 'configurable'])
      ->addAttributeToFilter('sku', ['like' => 'Bmin%'])
      ->addAttributeToFilter('entity_id', ['nin' => [$source_id]]);

    printf("Found %d products\n", count($col));
    sleep(5);

    /** @var Mage_Catalog_Model_Product $product */
    foreach($col as $product) {
      printf("Updating '%d - %s'\n", $product->getId(), $product->getName());

      foreach ($product->getProductOptionsCollection() as $opt) {
        $opt->delete();
      }

      $optionInstance = $product->getOptionInstance();
      $optionInstance->unsetOptions();
      $optionInstance->addOption($new_option);
      $product->save();
    }
  }
}

$shell = new JardinsAvalon_Shell_FinishingKnotOption();
$shell->run();

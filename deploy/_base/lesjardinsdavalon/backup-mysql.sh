#!/bin/bash

USER="backup"
GROUP=$(id -gn $USER)

BASE_DATE=$(date -d 'yesterday')

BACKUP_FOLDER=${BACKUP_FOLDER:-"/var/backups"}
BACKUP_FILE_PREFIX=${BACKUP_FILE_PREFIX:-"lesjardinsdavalon-"}
BACKUP_HOST=${BACKUP_HOST:-"localhost"}
BACKUP_USER_PASSWORD=${BACKUP_USER_PASSWORD:-"changeme"}
BACKUP_DB=${BACKUP_DB:-"aourell"}
BACKUP_USER=${BACKUP_USER:-"backup"}

TEE_COMMAND="tee"

function check_file {
  FILE=$1
  if ! test -w $FILE; then
    touch $FILE
    chown $USER:$GROUP $FILE
    chmod 644 $FILE
  fi
}

function add_file_to_output {
  check_file $1
  TEE_COMMAND="$TEE_COMMAND $1"
}

# Daily backup
echo "Daily backup... YES"
add_file_to_output "${BACKUP_FOLDER}/${BACKUP_FILE_PREFIX}$(date -d "$BASE_DATE" '+%A').sql.gz"

# Weekly backup
echo -n "Weekly backup... "
if [ $(date '+%u') == '1' ]; then
  echo "YES"
  add_file_to_output "${BACKUP_FOLDER}/${BACKUP_FILE_PREFIX}week-$(date -d "$BASE_DATE" '+%W').sql.gz"
else
  echo "NO"
fi

# Monthly backup
echo -n "Monthly backup... "
if [ $(date '+%d') == '01' ]; then
  echo "YES"
  add_file_to_output "${BACKUP_FOLDER}/${BACKUP_FILE_PREFIX}$(date -d "$BASE_DATE" '+%B').sql.gz"
else
  echo "NO"
fi

# Yearly backup
echo -n "Yearly backup... "
if [ $(date '+%j') == '001' ]; then
  echo "YES"
  add_file_to_output "${BACKUP_FOLDER}/${BACKUP_FILE_PREFIX}$(date -d "$BASE_DATE" '+%Y').sql.gz"
else
  echo "NO"
fi

mysql ${BACKUP_DB} -h ${BACKUP_HOST} -u ${BACKUP_USER} -p${BACKUP_USER_PASSWORD} -e "show tables like 'magento_%'" | grep -v Tables_in \
  | xargs mysqldump ${BACKUP_DB} -h ${BACKUP_HOST} -u ${BACKUP_USER} -p${BACKUP_USER_PASSWORD} \
  | gzip | $TEE_COMMAND > /dev/null
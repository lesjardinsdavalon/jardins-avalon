data "kustomization_overlay" "configs" {
  kustomize_options = {
    enable_helm = true
  }

  resources = [
    "kustomize"
  ]
}

resource "kustomization_resource" "p0" {
  for_each = data.kustomization_overlay.configs.ids_prio[0]
  manifest = data.kustomization_overlay.configs.manifests[each.value]
}

resource "kustomization_resource" "p1" {
  for_each   = data.kustomization_overlay.configs.ids_prio[1]
  manifest   = data.kustomization_overlay.configs.manifests[each.value]
  depends_on = [kustomization_resource.p0]
}

resource "kustomization_resource" "p2" {
  for_each   = data.kustomization_overlay.configs.ids_prio[2]
  manifest   = data.kustomization_overlay.configs.manifests[each.value]
  depends_on = [kustomization_resource.p1]
}
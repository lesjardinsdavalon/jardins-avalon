terraform {
  required_providers {
    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = "2.6.1"
    }

    null = {
      source  = "hashicorp/null"
      version = "3.1.0"
    }

    kustomization = {
      source = "kbst/kustomization"
      version = "0.7.1"
    }

    random = {
      source = "hashicorp/random"
      version = "3.1.0"
    }

    local = {
      source = "hashicorp/local"
      version = "2.1.0"
    }
  }
}

provider "kustomization" {
  kubeconfig_path = "./kubeconfig.yml"
}


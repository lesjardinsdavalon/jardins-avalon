'use strict';

var LIVERELOAD_PORT = 35729;
module.exports = function (grunt) {

  // load all grunt tasks
  require('matchdep').filterDev('grunt-*').forEach(grunt.loadNpmTasks);

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),

    watch: {
      options: {
        nospawn: true
      },
      less: {
        files: [
          "skin/**/*.less"
        ],
        tasks: ['less']
      },
      magento: {
        files: [
          "app/code/local/**/*.php",
          "app/design/*/ljda/**/*.*",
          "app/locale/**/JardinsAvalon_*.csv",
          "skin/*/ljda/**/*.css",
          "skin/*/ljda/**/*.js",
          "errors/**/*.*"
        ],
        tasks: [],
        options: {
          livereload: LIVERELOAD_PORT
        }
      }
    },

    less: {
      core: {
        options: {
          strictMath: true,
          sourceMap: true,
          outputSourceFiles: true
        },
        files: {
          "skin/frontend/ljda/default/css/main.css": "skin/frontend/ljda/default/css/main.less"
        }
      }
    },

    clean: {
      dist: ['dist']
    },

    copy: {
      dist: {
        files: [
          {
            expand: true,
            src: [
              "*.php",
              "cron*",
              "LICENSE*",
              "mage",
              "favicon*",
              "robots.txt",
              "app/**/*",
              "downloader/**/*",
              "errors/**/*",
              "includes/**/*",
              "js/**/*",
              "lib/**/*",
              "media/.htaccess",
              "pkginfo/**/*",
              "shell/**/*",
              "skin/**/*",
              "var/.htaccess",
              "!install.php",
              "!app/etc/local.xml"
            ],
            filter: 'isFile',
            dest: 'dist/<%= pkg.name %>-<%= pkg.version %>'
          }
        ]
      }
    },

    compress: {
      dist: {
        options: {
          archive: 'dist/<%= pkg.name %>-<%= pkg.version %>.zip'
        },
        expand: true,
        dot: true,
        cwd: 'dist/',
        src: ['**/*']
      }
    }

  });

  grunt.registerTask('default', ['watch']);

  grunt.registerTask('package', ['clean', 'copy', 'compress']);

};
<?php
/**
 * Les Jardins d'Avalon
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@lesjardinsdavalon.fr so we can send you a copy immediately.
 *
 * @category    JardinsAvalon
 * @package     JardinsAvalon_Catalog
 * @copyright   Copyright (c) 2013 Les Jardins d'Avalon (http://www.lesjardinsdavalon.fr)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Description of JardinsAvalon_Catalog_Helper_Product_Compare
 *
 * @category    JardinsAvalon
 * @package     JardinsAvalon_Catalog
 * @author      Pierre-Gildas MILLON <pg.millon@gmail.com>
 */
class JardinsAvalon_Catalog_Helper_Product_Compare extends Mage_Catalog_Helper_Product_Compare {

  /**
   * Retrieve url for adding product to conpare list, return false
   * @param   Mage_Catalog_Model_Product $product - not used but must leave else we get errors
   * @return  boolean false
   */
  public function getAddUrl($product) {
    return false;
  }
  
}

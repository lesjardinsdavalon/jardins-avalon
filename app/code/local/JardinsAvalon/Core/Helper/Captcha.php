<?php
/**
 * (C) Copyright 2016 Les Jardins d'Avalon
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@lesjardinsdavalon.fr so we can send you a copy immediately.
 */

class JardinsAvalon_Core_Helper_Captcha extends Mage_Core_Helper_Abstract {

    public function isCaptchaValid() {
        /** @var Mage_Core_Model_Session $session */
        $validResponses = ['le', 'les', 'jardin', 'jardins', "d'", 'de', 'd', 'avalon', 'avallon'];

        return in_array(strtolower($this->_getRequest()->getPost('captcha')), $validResponses);
    }

}
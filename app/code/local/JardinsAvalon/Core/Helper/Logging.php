<?php
/**
 * (C) Copyright 2016 Les Jardins d'Avalon
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@lesjardinsdavalon.fr so we can send you a copy immediately.
 */

/**
 * Description of JardinsAvalon_Core_Helper_Logging
 *
 * @category    JardinsAvalon
 * @package     JardinsAvalon_Core
 * @author      Pierre-Gildas MILLON <pgmillon@gmail.com>
 */

use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use Monolog\Formatter\LogstashFormatter;

class JardinsAvalon_Core_Helper_Logging extends Mage_Core_Helper_Abstract {

  const XML_PATH_ACTIVE   = 'ljda/customer_logging/active';
  const XML_PATH_LOG_PATH = 'ljda/customer_logging/log_path';

  protected $logger;

  public function __construct() {
    $formatter = new LogstashFormatter('magento_customer_log', null, null, 'ctxt_', LogstashFormatter::V1);

    $logDir  = Mage::getBaseDir('var').DS.'log'.DS;

    $handler = new StreamHandler($logDir.$this->getCustomerLoggingPath());
    $handler->setFormatter($formatter);

    $this->logger = new Logger('ljda');
    $this->logger->pushHandler($handler);
  }

  /**
   * @param mixed $store
   * @return bool
   */
  public function isCustomerLoggingActive($store = null) {
    return Mage::getStoreConfigFlag(self::XML_PATH_ACTIVE, $store);
  }

  public function getCustomerLoggingPath($store = null) {
    return Mage::getStoreConfig(self::XML_PATH_LOG_PATH, $store);
  }

  /**
   * @return Logger
   */
  public function getLogger() {
    return $this->logger;
  }

  public function getContextData() {
    return array_merge(
      $this->getRequestContextData(),
      $this->getCustomerContextData(),
      $this->getCheckoutContextData()
    );
  }

  protected function getCustomerContextData() {
    /** @var Mage_Customer_Model_Session $session */
    $session = Mage::getSingleton('customer/session');

    return $session->isLoggedIn() ? array(
      'customer_id' => $session->getCustomerId()
    ) : array();
  }

  protected function getRequestContextData() {
    parse_str($this->_getRequest()->getRawBody(), $out);
    $replacements = array();

    if($out) {
      $iterator = new RecursiveIteratorIterator(new RecursiveArrayIterator($out));
      foreach($iterator as $key => $value) {
        if(in_array($key, array('password', 'current_password', 'confirmation'), true)) {
          $replacements[] = $value;
        }
      }
    }

    return array(
      'request_headers' => array_filter($_SERVER, function($key) {
        return is_string($key) && strpos($key, 'HTTP_') === 0;
      }, ARRAY_FILTER_USE_KEY),
      'request_uri' => $this->_getRequest()->getRequestUri(),
      'request_method' => $this->_getRequest()->getMethod(),
      'request_scheme' => $this->_getRequest()->getScheme(),
      'request_host' => $this->_getRequest()->getHttpHost(),
      'request_body' => str_replace($replacements, '*******', $this->_getRequest()->getRawBody())
    );
  }

  protected function getCheckoutContextData() {
    /** @var Mage_Checkout_Model_Session $session */
    $session = Mage::getSingleton('checkout/session');

    return $session->getQuoteId() ? array(
      'quote_id' => $session->getQuoteId(),
      'quote_products' => array_map(function($item) {
        return array(
          'product_id' => $item->getProductId(),
          'product_qty' => $item->getQty()
        );
      }, $session->getQuote()->getAllItems())
    ) : array();
  }

}


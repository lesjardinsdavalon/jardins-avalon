<?php
/**
 * (C) Copyright 2016 Les Jardins d'Avalon
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@lesjardinsdavalon.fr so we can send you a copy immediately.
 */

class JardinsAvalon_Core_Model_Observer {

  protected static $STORAGE_TYPES = array(
    'admin/session',
    'adminhtml/session',
    'core/session',
    'catalog/session',
    'checkout/session',
    'customer/session',
    'newsletter/session',
    'paypal/session',
    'review/session'
  );

  protected static $MESSAGE_TYPES = array(
    Mage_Core_Model_Message::ERROR,
    Mage_Core_Model_Message::NOTICE,
    Mage_Core_Model_Message::SUCCESS,
    Mage_Core_Model_Message::WARNING
  );

  protected $messageCache = array();

  public function addCustomerSessionMessage() {
    /** @var JardinsAvalon_Core_Helper_Logging $helper */
    $helper = Mage::helper('jardinsavalon_core/logging');

    if($helper->isCustomerLoggingActive()) {
      $messages = array();

      foreach(self::$STORAGE_TYPES as $storageName) {
        /** @var Mage_Core_Model_Session_Abstract $storage */
        $storage = Mage::getSingleton($storageName);
        if ($storage) {
          foreach(self::$MESSAGE_TYPES as $type) {
            $messages[] = array_map(function($item) use ($storageName, $type) {
              return new JardinsAvalon_Core_Model_Message($item, $storageName, $type);
            }, $storage->getMessages()->getItems($type));
          }
        }
      }
      $messages = array_merge(...$messages);

      if($messages) {
        /** @var JardinsAvalon_Core_Model_Message $message */
        foreach($messages as $message) {
          $hash = spl_object_hash($message);
          if(!in_array($hash, $this->messageCache, true)) {
            $helper->getLogger()->info($message->getText(), array_merge(array(
              'msg_storage' => $message->getStorage(),
              'msg_type' => $message->getType()
            ), $helper->getContextData()));
            $this->messageCache[] = $hash;
          }
        }
      }

    }
  }
}

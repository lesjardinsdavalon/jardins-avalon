<?php
/**
 * Les Jardins d'Avalon
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@lesjardinsdavalon.fr so we can send you a copy immediately.
 *
 * @category    JardinsAvalon
 * @package     JardinsAvalon_AisleEnd
 * @copyright   Copyright (c) 2013 Les Jardins d'Avalon (http://www.lesjardinsdavalon.fr)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Description of JardinsAvalon_AisleEnd_Block_Announces
 *
 * @category    JardinsAvalon
 * @package     JardinsAvalon_Core
 * @author      Pierre-Gildas MILLON <pg.millon@gmail.com>
 */
class JardinsAvalon_Core_Block_Callout extends Mage_Core_Block_Template {

  /**
   * @var Mage_Cms_Block_Block
   */
  private $childBlock;

  public function _construct() {
    $this->setTemplate('core/callout.phtml');

    parent::_construct();
  }

  protected function _prepareLayout() {
    $this->setChild('callout.content',
      $this->childBlock = $this->getLayout()->createBlock('cms/block')
    );

    return parent::_prepareLayout();
  }

  protected function _beforeToHtml() {
    $this->childBlock->setBlockId($this->block_id);

    return parent::_beforeToHtml();
  }

}

<?php

/*
 * Les Jardins d'Avalon
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@lesjardinsdavalon.fr so we can send you a copy immediately.
 * 
 * @category    JardinsAvalon
 * @package     JardinsAvalon_Payment
 * @copyright   Copyright (c) 2013 Les Jardins d'Avalon (http://www.lesjardinsdavalon.fr)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Description of JardinsAvalon_Payment_Model_Config
 *
 * @category    JardinsAvalon
 * @package     JardinsAvalon_Payment
 * @author Pierre-Gildas MILLON <pg.millon@gmail.com>
 */
class JardinsAvalon_Payment_Model_Config extends Mage_Paypal_Model_Config {

  const METHOD_LJDA_CC = 'ljda_cc';

  public function isMethodAvailable($methodCode = null) {
    $result = parent::isMethodAvailable($methodCode);
    
    // check for direct payments dependence
    if($this->isMethodActive(self::METHOD_WPP_PE_DIRECT)) {
      $result = false;
    } elseif($this->isMethodActive(self::METHOD_WPP_DIRECT)) {
      $result = true;
    }
    
    return $result;
  }
  
  protected function _prepareValue($key, $value) {
    $value = parent::_prepareValue($key, $value);

    // Always set payment action as "Sale" for Unilateral payments in EC
    if($key == 'payment_action' 
     && $value != self::PAYMENT_ACTION_SALE 
     && $this->_methodCode == self::METHOD_WPP_EXPRESS 
     && $this->shouldUseUnilateralPayments()) {
      return self::PAYMENT_ACTION_SALE;
    }
    
    return $value;
  }

  public function getCountryMethods($countryCode = null) {
    $countryMethods = parent::getCountryMethods($countryCode);
    $countryMethods[] = self::METHOD_LJDA_CC;
    return $countryMethods;
  }

  public function getPaymentActions() {
    $paymentActions = parent::getPaymentActions();
    $paymentActions[self::PAYMENT_ACTION_ORDER] = Mage::helper('paypal')->__('Order');
    return $paymentActions;
  }

  protected function _getSpecificConfigPath($fieldName) {
    $path = $this->_mapExpressFieldset($fieldName);

    if($path === null) {
      $path = $this->_mapWppFieldset($fieldName);
    }

    if($path === null) {
      parent::_getSpecificConfigPath($fieldName);
    }

    return $path;
  }

}

<?php
/**
 * Les Jardins d'Avalon
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@lesjardinsdavalon.fr so we can send you a copy immediately.
 *
 * @category    JardinsAvalon
 * @package     JardinsAvalon_Payment
 * @copyright   Copyright (c) 2013 Les Jardins d'Avalon (http://www.lesjardinsdavalon.fr)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Description of JardinsAvalon_Payment_Model_Method_Site
 *
 * @category    JardinsAvalon
 * @package     JardinsAvalon_Payment
 * @author      Pierre-Gildas MILLON <pg.millon@gmail.com>
 */
class JardinsAvalon_Payment_Model_Method_Site extends Mage_Payment_Model_Method_Abstract {

  protected $_code = 'onsite';
  protected $_formBlockType = 'JardinsAvalon_Payment_Block_Form_Site';
  protected $_infoBlockType = 'JardinsAvalon_Payment_Block_Info_Site';

  /**
   * Assign data to info model instance
   *
   * @param   mixed $data
   * @return  JardinsAvalon_Model_Method_Site
   */
  public function assignData($data) {
    $details = array();
    if($this->getDetails()) {
      $details['details'] = $this->getDetails();
    }
    if(!empty($details)) {
      $this->getInfoInstance()->setAdditionalData(serialize($details));
    }
    return $this;
  }

  public function getDetails() {
    return $this->getConfigData('details');
  }

}

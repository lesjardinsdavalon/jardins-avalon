<?php

/*
 * Les Jardins d'Avalon
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@lesjardinsdavalon.fr so we can send you a copy immediately.
 * 
 * @category    JardinsAvalon
 * @package     JardinsAvalon_Payment
 * @copyright   Copyright (c) 2013 Les Jardins d'Avalon (http://www.lesjardinsdavalon.fr)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Description of JardinsAvalon_Payment_Model_Method_Express
 *
 * @category    JardinsAvalon
 * @package     JardinsAvalon_Payment
 * @author Pierre-Gildas MILLON <pg.millon@gmail.com>
 */
class JardinsAvalon_Payment_Model_Method_Express extends Mage_Paypal_Model_Express {

  protected $_code          = 'ljda_cc';
  protected $_formBlockType = 'JardinsAvalon_Payment_Block_Form_CreditCard';
  protected $_proType       = 'JardinsAvalon_Payment_Model_Pro';

  public function __construct($params = array()) {
    parent::__construct($params);
    
    $this->_pro->setMethod('ljda_cc');
  }
  
  public function isAvailable($quote = null) {
    return parent::isAvailable($quote);
  }
  
}

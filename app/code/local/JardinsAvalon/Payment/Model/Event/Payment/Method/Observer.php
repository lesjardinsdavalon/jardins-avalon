<?php
/**
 * Les Jardins d'Avalon
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@lesjardinsdavalon.fr so we can send you a copy immediately.
 *
 * @category    JardinsAvalon
 * @package     JardinsAvalon_Payment
 * @copyright   Copyright (c) 2013 Les Jardins d'Avalon (http://www.lesjardinsdavalon.fr)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Description of JardinsAvalon_Payment_Model_Event_Payment_Method_Observer
 *
 * @category    JardinsAvalon
 * @package     JardinsAvalon_Payment
 * @author      Pierre-Gildas MILLON <pg.millon@gmail.com>
 */
class JardinsAvalon_Payment_Model_Event_Payment_Method_Observer {
  
  /**
   * 
   * @param Varien_Event_Observer $observer
   */
  public function alterCheckMoPaymentWhenShippingMethodIsPickup($observer) {
    $event          = $observer->getEvent();
    $paymentMethod  = $event->getData('method_instance');
    $quote          = $observer->getQuote();
    /* @var $quote Mage_Sales_Model_Quote */

    if(!empty($quote)) {
      $shippingAddress = $quote->getShippingAddress();
      $shippingMethod = $shippingAddress->getShippingMethod();

      $shippingRate = $shippingAddress->getShippingRateByCode($shippingMethod);
      if(!empty($shippingMethod) && !empty($shippingRate)) {
        $carrierInstance = $shippingRate->getCarrierInstance();
        if($carrierInstance instanceof JardinsAvalon_Shipping_Model_Carrier_Pickup) {
          if(!$paymentMethod instanceof JardinsAvalon_Payment_Model_Method_Site) {
            $event->getResult()->isAvailable = false;
          }
        } else {
          if($paymentMethod instanceof JardinsAvalon_Payment_Model_Method_Site) {
            $event->getResult()->isAvailable = false;
          }
        }
      }
    }
  }
}

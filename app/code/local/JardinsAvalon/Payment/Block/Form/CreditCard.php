<?php

/*
 * Les Jardins d'Avalon
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@lesjardinsdavalon.fr so we can send you a copy immediately.
 * 
 * @category    JardinsAvalon
 * @package     JardinsAvalon_Payment
 * @copyright   Copyright (c) 2013 Les Jardins d'Avalon (http://www.lesjardinsdavalon.fr)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Description of Form
 *
 * @category    JardinsAvalon
 * @package     JardinsAvalon_Payment
 * @author Pierre-Gildas MILLON <pg.millon@gmail.com>
 */
class JardinsAvalon_Payment_Block_Form_CreditCard extends Mage_Paypal_Block_Express_Form {

  /**
   * Payment method code
   * @var string
   */
  protected $_methodCode = 'ljda_cc';

  /**
   * Set template and redirect message
   */
  protected function _construct() {
    $this->_config = Mage::getModel('JardinsAvalon_Payment_Model_Config')->setMethod($this->getMethodCode());
    
    $locale = Mage::app()->getLocale();
    
    $clazz = Mage::getConfig()->getBlockClassName('core/template');
    $mark = new $clazz;
    
    $mark->setTemplate('payment/form/paypal.phtml');

    $this->setTemplate('paypal/payment/redirect.phtml');
    $this->setMethodTitle('');
    $this->setMethodLabelAfterHtml($mark->toHtml());
    $this->setRedirectMessage(Mage::helper('paypal')->__('You will be redirected to our partner website. You will ba able to pay by credit card without havin a Paypal account.'));
    
    return Mage_Payment_Block_Form::_construct();
  }

}

<?php
/**
 * (C) Copyright 2016 Les Jardins d'Avalon
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@lesjardinsdavalon.fr so we can send you a copy immediately.
 */

require_once Mage::getModuleDir('controllers','Mage_Newsletter').DS.'SubscriberController.php';

class JardinsAvalon_Newsletter_SubscriberController extends Mage_Newsletter_SubscriberController {

  public function newAction() {
    $checks = array(
      'isPostRequest',
      'isFormKeyValid',
      'isRequestWithEmailField',
      'isRefererValid'
    );

    /** @var Mage_Core_Model_Session $session */
    $session = Mage::getSingleton('core/session');

    /** @var \Monolog\Logger $logger */
    $logger = Mage::helper('jardinsavalon_core/logging')->getLogger();

    $logger->info('New subscription request', Mage::helper('jardinsavalon_core/logging')->getContextData());

    foreach($checks as $check) {
      if(!$this->$check()) {
        $e = new Mage_Core_Exception($this->__('Invalid form, please try submitting form again.'));
        $session->addException($e, $this->__('There was a problem with the subscription: %s', $e->getMessage()));

        $this->_redirectReferer();
        return;
      }
    }

    parent::newAction();
  }

  protected function isFormKeyValid() {
    /** @var Mage_Core_Model_Session $session */
    $session = Mage::getSingleton('core/session');

    return $session->validateFormKey($this->getRequest()->getPost(Mage_Core_Model_Url::FORM_KEY));
  }

  protected function isPostRequest() {
    return $this->getRequest()->isPost();
  }

  protected function isRequestWithEmailField() {
    return $this->getRequest()->getPost('email');
  }

  protected function isRefererValid() {
    $secure_base_url = Mage::getStoreConfig(Mage_Core_Model_Store::XML_PATH_SECURE_BASE_URL);
    $unsecure_base_url = Mage::getStoreConfig(Mage_Core_Model_Store::XML_PATH_UNSECURE_BASE_URL);

    return (strpos($this->getRequest()->getHeader('referer'), $secure_base_url) === 0) ||
      (strpos($this->getRequest()->getHeader('referer'), $unsecure_base_url) === 0);
  }

}


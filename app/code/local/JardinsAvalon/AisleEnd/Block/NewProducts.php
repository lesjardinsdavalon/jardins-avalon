<?php
/**
 * Les Jardins d'Avalon
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@lesjardinsdavalon.fr so we can send you a copy immediately.
 *
 * @category    JardinsAvalon
 * @package     JardinsAvalon_AisleEnd
 * @copyright   Copyright (c) 2013 Les Jardins d'Avalon (http://www.lesjardinsdavalon.fr)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Description of JardinsAvalon_AisleEnd_Block_NewProducts
 *
 * @category    JardinsAvalon
 * @package     JardinsAvalon_AisleEnd
 * @author      Pierre-Gildas MILLON <pg.millon@gmail.com>
 */
class JardinsAvalon_AisleEnd_Block_NewProducts extends Mage_Catalog_Block_Product_New {

  /**
   * Get Key pieces for caching block content
   *
   * @return array
   */
  public function getCacheKeyInfo()
  {
    return array(
      'JARDINSAVALON_AISLEEND_NEWPRODUCTS',
      Mage::app()->getStore()->getId(),
      Mage::getDesign()->getPackageName(),
      Mage::getDesign()->getTheme('template'),
      Mage::getSingleton('customer/session')->getCustomerGroupId(),
      'template' => $this->getTemplate(),
      $this->getProductsCount()
    );
  }

  protected function _beforeToHtml() {

    $collection = Mage::getResourceModel('catalog/product_collection');
    $collection->setVisibility(Mage::getSingleton('catalog/product_visibility')->getVisibleInCatalogIds());

    $collection->getSelect()
      ->joinRight(
        array('aisle_end_product' => $collection->getTable('aisle_end/newProduct')),
        'aisle_end_product.mage_product_id = e.entity_id')
      ->order('aisle_end_product.position');

    $collection = $this->_addProductAttributesAndPrices($collection)
      ->addStoreFilter()
      ->setPageSize($this->getProductsCount())
      ->setCurPage(1);

    $this->setProductCollection($collection);

    return $this;
  }
}

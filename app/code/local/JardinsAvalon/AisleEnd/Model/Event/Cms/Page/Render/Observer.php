<?php
/**
 * Les Jardins d'Avalon
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@lesjardinsdavalon.fr so we can send you a copy immediately.
 *
 * @category    JardinsAvalon
 * @package     JardinsAvalon_AisleEnd
 * @copyright   Copyright (c) 2013 Les Jardins d'Avalon (http://www.lesjardinsdavalon.fr)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Description of JardinsAvalon_AisleEnd_Model_Event_Cms_Page_Render_Observer
 *
 * @category    JardinsAvalon
 * @package     JardinsAvalon_AisleEnd
 * @author      Pierre-Gildas MILLON <pg.millon@gmail.com>
 */
class JardinsAvalon_AisleEnd_Model_Event_Cms_Page_Render_Observer {
  
  /**
   * 
   * @param Varien_Event_Observer $observer
   */
  public function addHomePageLayoutHandle($observer) {
    $event = $observer->getEvent();
    $page = $event->getPage();
    $action = $event->getControllerAction();
    
    /* @var $page Mage_Cms_Model_Page */
    /* @var $action Mage_Cms_IndexController */
    
    if($page->getIdentifier() == 'home') {
      $action->getLayout()->getUpdate()->addHandle('cms_page_home');
    }
  }
  
}

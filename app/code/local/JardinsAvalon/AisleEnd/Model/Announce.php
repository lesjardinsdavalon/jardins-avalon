<?php
/**
 * Les Jardins d'Avalon
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@lesjardinsdavalon.fr so we can send you a copy immediately.
 *
 * @category    JardinsAvalon
 * @package     JardinsAvalon_AisleEnd
 * @copyright   Copyright (c) 2013 Les Jardins d'Avalon (http://www.lesjardinsdavalon.fr)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Description of JardinsAvalon_AisleEnd_Model_Announce
 *
 * @category    JardinsAvalon
 * @package     JardinsAvalon_AisleEnd
 * @author      Pierre-Gildas MILLON <pg.millon@gmail.com>
 */
class JardinsAvalon_AisleEnd_Model_Announce extends Mage_Core_Model_Abstract {
  
  const IMAGE_SUBFOLDER = 'aisle_end';
  
  public function _construct() {
    $this->_init('aisle_end/announce');
  }
  
  public function getImageUrl() {
    return Mage::getBaseUrl('media').self::IMAGE_SUBFOLDER.'/'.$this->getData('image_name');
  }
  
  public function getImagePath() {
    return $this->getBaseImagePath().$this->getData('image_name');
  }
  
  public function getBaseImagePath() {
    return Mage::getBaseDir('media').DIRECTORY_SEPARATOR.self::IMAGE_SUBFOLDER.DIRECTORY_SEPARATOR;
  }
  
}

<?php
/* 
 * Les Jardins d'Avalon
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@lesjardinsdavalon.fr so we can send you a copy immediately.
 *
 * @category    JardinsAvalon
 * @package     JardinsAvalon_AisleEnd
 * @copyright   Copyright (c) 2013 Les Jardins d'Avalon (http://www.lesjardinsdavalon.fr)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

$installer = $this;
/* @var $installer JardinsAvalon_AisleEnd_Model_Resource_Setup */

$installer->startSetup();

$installer->run("CREATE TABLE `{$installer->getTable('aisle_end/newProduct')}` (
  `product_id` int(11) NOT NULL AUTO_INCREMENT,
  `mage_product_id` int(10) NOT NULL,
  PRIMARY KEY (`product_id`),
  CONSTRAINT `FK_AISLEEND_NEW_PRD_MAGE_PRD_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`mage_product_id`) REFERENCES `magento_catalog_product_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE
);");

$installer->endSetup();
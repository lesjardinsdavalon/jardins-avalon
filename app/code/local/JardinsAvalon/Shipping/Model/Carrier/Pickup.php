<?php
/**
 * Les Jardins d'Avalon
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@lesjardinsdavalon.fr so we can send you a copy immediately.
 *
 * @category    JardinsAvalon
 * @package     JardinsAvalon_Shipping
 * @copyright   Copyright (c) 2013 Les Jardins d'Avalon (http://www.lesjardinsdavalon.fr)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Description of JardinsAvalon_Shipping_Model_Carrier_Pickup
 *
 * @category    JardinsAvalon
 * @package     JardinsAvalon_Shipping
 * @author      Pierre-Gildas MILLON <pg.millon@gmail.com>
 */
class JardinsAvalon_Shipping_Model_Carrier_Pickup extends Mage_Shipping_Model_Carrier_Pickup {

  /**
   * Collect and get rates
   *
   * @abstract
   * @param Mage_Shipping_Model_Rate_Request $request
   * @return Mage_Shipping_Model_Rate_Result|bool|null
   */
  public function collectRates(Mage_Shipping_Model_Rate_Request $request) {
    if(!$this->getConfigFlag('active')) {
      return false;
    }

    $result = Mage::getModel('shipping/rate_result');

    $method = Mage::getModel('shipping/rate_result_method');

    $method->setCarrier('pickup');
    $method->setCarrierTitle($this->getConfigData('title'));

    $method->setMethod('store');
    $method->setMethodTitle($this->getConfigData('name'));

    $method->setPrice(0);
    $method->setCost(0);

    $result->append($method);

    return $result;
  }

}

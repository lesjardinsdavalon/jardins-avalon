<?php


class JardinsAvalon_Sales_Model_Rule_Condition_Product_Subselect
  extends Mage_SalesRule_Model_Rule_Condition_Product_Subselect {

  public function validate(Varien_Object $object) {

    if ($object instanceof Mage_Sales_Model_Quote_Item) {
      return Mage_SalesRule_Model_Rule_Condition_Product_Combine::validate($object);
    }

    return parent::validate($object);
  }


}
<?php
/**
 * Les Jardins d'Avalon
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@lesjardinsdavalon.fr so we can send you a copy immediately.
 *
 * @category    JardinsAvalon
 * @package     JardinsAvalon_Sales
 * @copyright   Copyright (c) 2013 Les Jardins d'Avalon (http://www.lesjardinsdavalon.fr)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Description of JardinsAvalon_Sales_Model_Order_Pdf_Invoice
 *
 * @category    JardinsAvalon
 * @package     JardinsAvalon_Sales
 * @author      Pierre-Gildas MILLON <pg.millon@gmail.com>
 */
class JardinsAvalon_Sales_Model_Order_Pdf_Invoice extends JardinsAvalon_Sales_Model_Order_Pdf_Abstract {

  /**
   * Draw header for item tabled
   *
   * @param Zend_Pdf_Page $page
   * @return void
   */
  protected function _drawHeader(Zend_Pdf_Page $page) {
    /* Add table head */
    $this->_setFontRegular($page, 10);
    $page->setFillColor(new Zend_Pdf_Color_RGB(0.93, 0.92, 0.92));
    $page->setLineColor(new Zend_Pdf_Color_GrayScale(0.5));
    $page->setLineWidth(0.5);
    $page->drawRectangle(25, $this->y, 570, $this->y - 15);
    $this->y -= 10;
    $page->setFillColor(new Zend_Pdf_Color_RGB(0, 0, 0));

    //columns headers
    $lines[0][] = array(
      'text' => Mage::helper('sales')->__('Products'),
      'feed' => 35
    );

    $lines[0][] = array(
      'text' => Mage::helper('sales')->__('SKU'),
      'feed' => 290,
      'align' => 'right'
    );

    $lines[0][] = array(
      'text' => Mage::helper('sales')->__('Qty'),
      'feed' => 435,
      'align' => 'right'
    );

    $lines[0][] = array(
      'text' => Mage::helper('sales')->__('Price'),
      'feed' => 360,
      'align' => 'right'
    );

    $lines[0][] = array(
      'text' => Mage::helper('sales')->__('Tax'),
      'feed' => 495,
      'align' => 'right'
    );

    $lines[0][] = array(
      'text' => Mage::helper('sales')->__('Subtotal'),
      'feed' => 565,
      'align' => 'right'
    );

    $lineBlock = array(
      'lines' => $lines,
      'height' => 5
    );

    $this->drawLineBlocks($page, array($lineBlock), array('table_header' => true));
    $page->setFillColor(new Zend_Pdf_Color_GrayScale(0));
    $this->y -= 20;
  }

  /**
   * Return PDF document
   *
   * @param  array $invoices
   * @return Zend_Pdf
   */
  public function getPdf($invoices = array()) {
    $this->_beforeGetPdf();
    $this->_initRenderer('invoice');

    $pdf = new Zend_Pdf();
    $this->_setPdf($pdf);
    $style = new Zend_Pdf_Style();
    $this->_setFontBold($style, 10);

    foreach($invoices as $invoice) {
      if($invoice->getStoreId()) {
        Mage::app()->getLocale()->emulate($invoice->getStoreId());
        Mage::app()->setCurrentStore($invoice->getStoreId());
      }
      $page = $this->newPage();
      $order = $invoice->getOrder();
      /* Add image */
      $this->insertLogo($page, $invoice->getStore());
      /* Add address */
      $this->insertAddress($page, $invoice->getStore());
      /* Add head */
      $this->insertOrder(
       $page, $order, Mage::getStoreConfigFlag(self::XML_PATH_SALES_PDF_INVOICE_PUT_ORDER_ID, $order->getStoreId())
      );
      /* Add document text and number */
      $this->insertDocumentNumber(
       $page, Mage::helper('sales')->__('Invoice # ') . $invoice->getIncrementId() . Mage::helper('sales')->__(' issued on ') . Mage::helper('core')->formatDate(
          $invoice->getCreatedAt(), 'medium', false)
      );
      /* Add table */
      $this->_drawHeader($page);
      /* Add body */
      foreach($invoice->getAllItems() as $item) {
        if($item->getOrderItem()->getParentItem()) {
          continue;
        }
        /* Draw item */
        $this->_drawItem($item, $page, $order);
        $page = end($pdf->pages);
      }
      /* Add totals */
      $page = $this->insertTotals($page, $invoice);

      $this->y -= 10;

      $page = $this->drawLineBlocks($page, array(array(
        'lines'  => array(array(
          array(
            'text'      => Mage::helper('sales')->__('Pay cash on receive.'),
            'feed'      => 565,
            'align'     => 'right',
            'font_size' => 10,
          ),
        )),
        'height' => 15
      )));

      $this->insertThanks($page);

      $this->insertFooter($page);

      if($invoice->getStoreId()) {
        Mage::app()->getLocale()->revert();
      }
    }
    $this->_afterGetPdf();
    return $pdf;
  }

  protected function insertShipments(&$page, $order, $shipment, $y, $topMargin) {
    $yShipments = $y;
    $totalShippingChargesText = "(" . Mage::helper('sales')->__('Total Shipping Charges') . " "
      . $order->formatPriceTxt($order->getShippingAmount()) . ")";

    $page->drawText($totalShippingChargesText, 285, $yShipments - $topMargin, 'UTF-8');
    $yShipments -= $topMargin + 10;

    $tracks = array();
    if($shipment) {
      $tracks = $shipment->getAllTracks();
    }
    if(count($tracks)) {
      $page->setFillColor(new Zend_Pdf_Color_Rgb(0.93, 0.92, 0.92));
      $page->setLineWidth(0.5);
      $page->drawRectangle(285, $yShipments, 510, $yShipments - 10);
      $page->drawLine(400, $yShipments, 400, $yShipments - 10);
      //$page->drawLine(510, $yShipments, 510, $yShipments - 10);

      $this->_setFontRegular($page, 9);
      $page->setFillColor(new Zend_Pdf_Color_GrayScale(0));
      //$page->drawText(Mage::helper('sales')->__('Carrier'), 290, $yShipments - 7 , 'UTF-8');
      $page->drawText(Mage::helper('sales')->__('Title'), 290, $yShipments - 7, 'UTF-8');
      $page->drawText(Mage::helper('sales')->__('Number'), 410, $yShipments - 7, 'UTF-8');

      $yShipments -= 20;
      $this->_setFontRegular($page, 8);
      foreach($tracks as $track) {

        $CarrierCode = $track->getCarrierCode();
        if($CarrierCode != 'custom') {
          $carrier = Mage::getSingleton('shipping/config')->getCarrierInstance($CarrierCode);
          $carrierTitle = $carrier->getConfigData('title');
        } else {
          $carrierTitle = Mage::helper('sales')->__('Custom Value');
        }

        //$truncatedCarrierTitle = substr($carrierTitle, 0, 35) . (strlen($carrierTitle) > 35 ? '...' : '');
        $maxTitleLen = 45;
        $endOfTitle = strlen($track->getTitle()) > $maxTitleLen ? '...' : '';
        $truncatedTitle = substr($track->getTitle(), 0, $maxTitleLen).$endOfTitle;
        //$page->drawText($truncatedCarrierTitle, 285, $yShipments , 'UTF-8');
        $page->drawText($truncatedTitle, 292, $yShipments, 'UTF-8');
        $page->drawText($track->getNumber(), 410, $yShipments, 'UTF-8');
        $yShipments -= $topMargin - 5;
      }
    } else {
      $yShipments -= $topMargin - 5;
    }

    return $yShipments;
  }

}

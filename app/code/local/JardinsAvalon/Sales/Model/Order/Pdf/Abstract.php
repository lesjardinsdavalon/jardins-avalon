<?php
/**
 * Les Jardins d'Avalon
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@lesjardinsdavalon.fr so we can send you a copy immediately.
 *
 * @category    JardinsAvalon
 * @package     JardinsAvalon_Sales
 * @copyright   Copyright (c) 2013 Les Jardins d'Avalon (http://www.lesjardinsdavalon.fr)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Description of JardinsAvalon_Sales_Model_Order_Pdf_Abstract
 *
 * @category    JardinsAvalon
 * @package     JardinsAvalon_Sales
 * @author      Pierre-Gildas MILLON <pg.millon@gmail.com>
 */
abstract class JardinsAvalon_Sales_Model_Order_Pdf_Abstract extends Mage_Sales_Model_Order_Pdf_Abstract
{

  protected $footerSize = 15;
  protected $pageWidth;
  protected $pageHeight;
  protected $pageWidthMm;
  protected $pageHeightMm;
  protected $pixelHeightMm;
  protected $pointHeightmm;

  protected static $PAGE_SIZE = Zend_Pdf_Page::SIZE_A4;
  protected static $PAGE_SIZE_MM = "210x297";
  protected static $POINTS_PER_INCH = 72;
  protected static $INCH_MM = 23.5;

  protected $footerBlock;
  protected $thanksBlock;

  protected $footerBlockFontSize = 8;
  protected $thanksBlockFontSize = 10;

  /**
   * @return Mage_Cms_Model_Block
   */
  public function getFooterBlock() {
    if($this->footerBlock == null) {
      $this->footerBlock = Mage::getModel('cms/block')->load('invoice-pdf_footer');
    }
    return $this->footerBlock;
  }

  /**
   * @return Mage_Cms_Model_Block
   */
  public function getThanksBlock() {
    if($this->thanksBlock == null) {
      $this->thanksBlock = Mage::getModel('cms/block')->load('invoice-pdf_thanks');
    }
    return $this->thanksBlock;
  }

  protected function calculateBlockSize($block, $fontSize) {
    $numLines = count(preg_split('/\n|\r/', $block->getContent()));
    $this->footerSize = $numLines * $fontSize * $this->pointHeightmm * $this->pixelHeightMm;
  }

  protected function insertBlock($page, $block, $fontSize) {
    foreach(preg_split('/\n|\r/', $block->getContent()) as $line) {
      $page->drawText($line, 25, $this->y, 'UTF-8');
      $this->y -= $fontSize * $this->pointHeightmm * $this->pixelHeightMm;
    }
  }

  protected function _beforeGetPdf()
  {
    parent::_beforeGetPdf();

    $pageDim = explode(':', self::$PAGE_SIZE);

    if(count($pageDim) == 2  ||  count($pageDim) == 3) {
      $this->pageWidth  = $pageDim[0];
      $this->pageHeight = $pageDim[1];
    } else {
      throw new Zend_Pdf_Exception('Wrong pagesize notation.');
    }

    $pageDim = explode('x', self::$PAGE_SIZE_MM);

    if(count($pageDim) == 2  ||  count($pageDim) == 3) {
      $this->pageWidthMm  = $pageDim[0];
      $this->pageHeightMm = $pageDim[1];
    } else {
      throw new Zend_Pdf_Exception('Wrong pagesize millimeter notation.');
    }

    $this->pixelHeightMm = $this->pageHeight / $this->pageHeightMm;
    $this->pointHeightmm = self::$INCH_MM / self::$POINTS_PER_INCH;

  }

  /**
   * Draw lines
   *
   * draw items array format:
   * lines        array;array of line blocks (required)
   * shift        int; full line height (optional)
   * height       int;line spacing (default 10)
   *
   * line block has line columns array
   *
   * column array format
   * text         string|array; draw text (required)
   * feed         int; x position (required)
   * font         string; font style, optional: bold, italic, regular
   * font_file    string; path to font file (optional for use your custom font)
   * font_size    int; font size (default 7)
   * align        string; text align (also see feed parametr), optional left, right
   * height       int;line spacing (default 10)
   *
   * @param  Zend_Pdf_Page $page
   * @param  array $draw
   * @param  array $pageSettings
   * @throws Mage_Core_Exception
   * @return Zend_Pdf_Page
   */
  public function drawLineBlocks(Zend_Pdf_Page $page, array $draw, array $pageSettings = array())
  {
    foreach($draw as $itemsProp) {
      if(!isset($itemsProp['lines']) || !is_array($itemsProp['lines'])) {
        Mage::throwException(Mage::helper('sales')->__('Invalid draw line data. Please define "lines" array.'));
      }
      $lines = $itemsProp['lines'];
      $height = isset($itemsProp['height']) ? $itemsProp['height'] : 10;

      if(empty($itemsProp['shift'])) {
        $shift = 0;
        foreach($lines as $line) {
          $maxHeight = 0;
          foreach($line as $column) {
            $lineSpacing = !empty($column['height']) ? $column['height'] : $height;
            if(!is_array($column['text'])) {
              $column['text'] = array($column['text']);
            }
            $top = 0;
            foreach($column['text'] as $part) {
              $top += $lineSpacing;
            }

            $maxHeight = $top > $maxHeight ? $top : $maxHeight;
          }
          $shift += $maxHeight;
        }
        $itemsProp['shift'] = $shift;
      }

      if($this->y - $itemsProp['shift'] < $this->footerSize) {
        $page = $this->newPage($pageSettings);
      }

      foreach($lines as $line) {
        $maxHeight = 0;
        foreach($line as $column) {
          $fontSize = empty($column['font_size']) ? 10 : $column['font_size'];
          if(!empty($column['font_file'])) {
            $font = Zend_Pdf_Font::fontWithPath($column['font_file']);
            $page->setFont($font, $fontSize);
          } else {
            $fontStyle = empty($column['font']) ? 'regular' : $column['font'];
            switch($fontStyle) {
              case 'bold':
                $font = $this->_setFontBold($page, $fontSize);
                break;
              case 'italic':
                $font = $this->_setFontItalic($page, $fontSize);
                break;
              default:
                $font = $this->_setFontRegular($page, $fontSize);
                break;
            }
          }

          if(!is_array($column['text'])) {
            $column['text'] = array($column['text']);
          }

          $lineSpacing = !empty($column['height']) ? $column['height'] : $height;
          $top = 0;
          foreach($column['text'] as $part) {
            if($this->y - $lineSpacing < $this->footerSize) {
              $page = $this->newPage($pageSettings);
            }

            $feed = $column['feed'];
            $textAlign = empty($column['align']) ? 'left' : $column['align'];
            $width = empty($column['width']) ? 0 : $column['width'];
            switch($textAlign) {
              case 'right':
                if($width) {
                  $feed = $this->getAlignRight($part, $feed, $width, $font, $fontSize);
                } else {
                  $feed = $feed - $this->widthForStringUsingFontSize($part, $font, $fontSize);
                }
                break;
              case 'center':
                if($width) {
                  $feed = $this->getAlignCenter($part, $feed, $width, $font, $fontSize);
                }
                break;
            }
            $page->drawText($part, $feed, $this->y - $top, 'UTF-8');
            $top += $lineSpacing;
          }

          $maxHeight = $top > $maxHeight ? $top : $maxHeight;
        }
        $this->y -= $maxHeight;
      }
    }

    return $page;
  }

  /**
   * Insert order to pdf page
   *
   * @param Zend_Pdf_Page $page
   * @param Mage_Sales_Model_Order $obj
   * @param bool $putOrderId
   */
  protected function insertOrder(&$page, $obj, $putOrderId = true)
  {
    if($obj instanceof Mage_Sales_Model_Order) {
      $shipment = null;
      $order = $obj;
    } elseif($obj instanceof Mage_Sales_Model_Order_Shipment) {
      $shipment = $obj;
      $order = $shipment->getOrder();
    }

    $this->y = $this->y ? $this->y : 815;
    $top = $this->y;

    $page->setFillColor(new Zend_Pdf_Color_GrayScale(0.45));
    $page->setLineColor(new Zend_Pdf_Color_GrayScale(0.45));
    $page->drawRectangle(25, $top, 570, $top - 90);
    $page->setFillColor(new Zend_Pdf_Color_GrayScale(1));
    $this->setDocHeaderCoordinates(array(25, $top, 570, $top - 90));
    $this->_setFontRegular($page, 10);

    if($putOrderId) {
      $page->drawText(
        Mage::helper('sales')->__('Order # ').$order->getRealOrderId(), 35, ($top -= 30), 'UTF-8'
      );
    }
    $page->drawText(
      Mage::helper('sales')->__('Order Date: ').Mage::helper('core')->formatDate(
        $order->getCreatedAtStoreDate(), 'medium', false
      ),
      35,
      ($top -= 15),
      'UTF-8'
    );

    $page->drawText(
      Mage::helper('sales')->__('Shipping Deadline: ').Mage::helper('core')->formatDate(
        $order->getCreatedAtStoreDate()->addMonth(1), 'medium', false
      ),
      35,
      ($top -= 15),
      'UTF-8'
    );

    $top -= 10;
    $page->setFillColor(new Zend_Pdf_Color_Rgb(0.93, 0.92, 0.92));
    $page->setLineColor(new Zend_Pdf_Color_GrayScale(0.5));
    $page->setLineWidth(0.5);
    $page->drawRectangle(25, $top, 275, ($top - 25));
    $page->drawRectangle(275, $top, 570, ($top - 25));

    /* Calculate blocks info */

    /* Billing Address */
    $billingAddress = $this->_formatAddress($order->getBillingAddress()->format('pdf'));

    /* Payment */
    $paymentInfo = Mage::helper('payment')->getInfoBlock($order->getPayment())
      ->setIsSecureMode(true)
      ->toPdf();
    $paymentInfo = htmlspecialchars_decode($paymentInfo, ENT_QUOTES);
    $payment = explode('{{pdf_row_separator}}', $paymentInfo);
    foreach($payment as $key => $value) {
      if(strip_tags(trim($value)) == '') {
        unset($payment[$key]);
      }
    }
    reset($payment);

    /* Shipping Address and Method */
    if(!$order->getIsVirtual()) {
      /* Shipping Address */
      $shippingAddress = $this->_formatAddress($order->getShippingAddress()->format('pdf'));
      $shippingMethod = $order->getShippingDescription();
    }

    $page->setFillColor(new Zend_Pdf_Color_GrayScale(0));
    $this->_setFontBold($page, 12);
    $page->drawText(Mage::helper('sales')->__('Sold to:'), 35, ($top - 15), 'UTF-8');

    if(!$order->getIsVirtual()) {
      $page->drawText(Mage::helper('sales')->__('Ship to:'), 285, ($top - 15), 'UTF-8');
    } else {
      $page->drawText(Mage::helper('sales')->__('Payment Method:'), 285, ($top - 15), 'UTF-8');
    }

    $addressesHeight = $this->_calcAddressHeight($billingAddress);
    if(isset($shippingAddress)) {
      $addressesHeight = max($addressesHeight, $this->_calcAddressHeight($shippingAddress));
    }

    $page->setFillColor(new Zend_Pdf_Color_GrayScale(1));
    $page->drawRectangle(25, ($top - 25), 570, $top - 33 - $addressesHeight);
    $page->setFillColor(new Zend_Pdf_Color_GrayScale(0));
    $this->_setFontRegular($page, 10);
    $this->y = $top - 40;
    $addressesStartY = $this->y;

    foreach($billingAddress as $value) {
      if($value !== '') {
        $text = array();
        foreach(Mage::helper('core/string')->str_split($value, 45, true, true) as $_value) {
          $text[] = $_value;
        }
        foreach($text as $part) {
          $page->drawText(strip_tags(ltrim($part)), 35, $this->y, 'UTF-8');
          $this->y -= 15;
        }
      }
    }

    $addressesEndY = $this->y;

    if(!$order->getIsVirtual()) {
      $this->y = $addressesStartY;
      foreach($shippingAddress as $value) {
        if($value !== '') {
          $text = array();
          foreach(Mage::helper('core/string')->str_split($value, 45, true, true) as $_value) {
            $text[] = $_value;
          }
          foreach($text as $part) {
            $page->drawText(strip_tags(ltrim($part)), 285, $this->y, 'UTF-8');
            $this->y -= 15;
          }
        }
      }

      $addressesEndY = min($addressesEndY, $this->y);
      $this->y = $addressesEndY;

      $page->setFillColor(new Zend_Pdf_Color_Rgb(0.93, 0.92, 0.92));
      $page->setLineWidth(0.5);
      $page->drawRectangle(25, $this->y, 275, $this->y - 25);
      $page->drawRectangle(275, $this->y, 570, $this->y - 25);

      $this->y -= 15;
      $this->_setFontBold($page, 12);
      $page->setFillColor(new Zend_Pdf_Color_GrayScale(0));
      $page->drawText(Mage::helper('sales')->__('Payment Method:'), 35, $this->y, 'UTF-8');
      $page->drawText(Mage::helper('sales')->__('Shipping Method:'), 285, $this->y, 'UTF-8');

      $this->y -= 10;
      $page->setFillColor(new Zend_Pdf_Color_GrayScale(1));

      $this->_setFontRegular($page, 10);
      $page->setFillColor(new Zend_Pdf_Color_GrayScale(0));

      $paymentLeft = 35;
      $yPayments = $this->y - 15;
    } else {
      $yPayments = $addressesStartY;
      $paymentLeft = 285;
    }

    foreach($payment as $value) {
      if(trim($value) != '') {
        //Printing "Payment Method" lines
        $value = preg_replace('/<br[^>]*>/i', "\n", $value);
        foreach(Mage::helper('core/string')->str_split($value, 45, true, true) as $_value) {
          $page->drawText(strip_tags(trim($_value)), $paymentLeft, $yPayments, 'UTF-8');
          $yPayments -= 15;
        }
      }
    }

    if($order->getIsVirtual()) {
      // replacement of Shipments-Payments rectangle block
      $yPayments = min($addressesEndY, $yPayments);
      $page->drawLine(25, ($top - 25), 25, $yPayments);
      $page->drawLine(570, ($top - 25), 570, $yPayments);
      $page->drawLine(25, $yPayments, 570, $yPayments);

      $this->y = $yPayments - 15;
    } else {
      $topMargin = 15;
      $methodStartY = $this->y;
      $this->y -= 15;

      foreach(Mage::helper('core/string')->str_split($shippingMethod, 45, true, true) as $_value) {
        $page->drawText(strip_tags(trim($_value)), 285, $this->y, 'UTF-8');
        $this->y -= 15;
      }

      $yShipments = $this->insertShipments($page, $order, $shipment, $this->y, $topMargin);

      $currentY = min($yPayments, $yShipments);

      // replacement of Shipments-Payments rectangle block
      $page->drawLine(25, $methodStartY, 25, $currentY); //left
      $page->drawLine(25, $currentY, 570, $currentY); //bottom
      $page->drawLine(570, $currentY, 570, $methodStartY); //right

      $this->y = $currentY;
      $this->y -= 15;
    }
  }

  /**
   * Insert logo to pdf page
   *
   * @param Zend_Pdf_Page $page
   * @param null $store
   */
  protected function insertLogo(&$page, $store = null)
  {
    $this->y = $this->y ? $this->y : 815;

    $logoFontPath = Mage::getModuleDir(null, 'JardinsAvalon_Core').'/skin/HARNGTON.TTF';
    $logoFont = Zend_Pdf_Font::fontWithPath($logoFontPath);

    $page->setFont($logoFont, 25);
    $page->drawText("Les Jardins d'Avalon", 25, $this->y, 'UTF-8');

    $this->y -= 25;
  }

  protected function insertShipments(&$page, $order, $shipment, $yShipments, $topMargin)
  {
    return $yShipments;
  }

  /**
   * Insert footer to pdf page
   *
   * @param  Zend_Pdf_Page $page
   * @return Zend_Pdf_Page
   */
  protected function insertFooter($page) {
    $this->y -= 15;

    $block = $this->getFooterBlock();

    $this->_setFontRegular($page, $this->footerBlockFontSize);

    $this->insertBlock($page, $block, $this->footerBlockFontSize);
  }

  /**
   * Insert thanks to pdf page
   *
   * @param  Zend_Pdf_Page $page
   * @return Zend_Pdf_Page
   */
  protected function insertThanks($page) {
    $this->y -= 5;

    $this->_setFontBold($page, $this->thanksBlockFontSize);

    $block = $this->getThanksBlock();

    $this->insertBlock($page, $block, $this->thanksBlockFontSize);
  }

  /**
   * Create new page and assign to PDF object
   *
   * @param  array $settings
   * @return Zend_Pdf_Page
   */
  public function newPage(array $settings = array()) {
    /* Add new table head */
    $page = $this->_getPdf()->newPage(self::$PAGE_SIZE);
    $this->_getPdf()->pages[] = $page;
    $this->y = 800;
    if(!empty($settings['table_header'])) {
      $this->_drawHeader($page);
    }
    return $page;
  }

}

<?php
/**
* (C) Copyright 2016 Les Jardins d'Avalon
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@lesjardinsdavalon.fr so we can send you a copy immediately.
*/

require_once Mage::getModuleDir('controllers','Mage_Customer').DS.'AccountController.php';

class JardinsAvalon_Customer_AccountController extends Mage_Customer_AccountController {

    public function createPostAction() {
        /** @var JardinsAvalon_Core_Helper_Captcha $captcha */
        $captcha = Mage::helper('jardinsavalon_core/captcha');

        if(!$captcha->isCaptchaValid()) {
            $session = $this->_getSession();

            $e = new Mage_Core_Exception($this->__('Invalid captcha, please try again.'));
            $session->addException($e, $this->__('There was a problem with the subscription: %s', $e->getMessage()));

            $this->_redirectError($this->_getUrl('*/*/create', array('_secure' => true)));

            return;
        }

        parent::createPostAction();
    }

}

<?php
/**
 * Les Jardins d'Avalon
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@lesjardinsdavalon.fr so we can send you a copy immediately.
 *
 * @category    JardinsAvalon
 * @package     JardinsAvalon_GiftMessage
 * @copyright   Copyright (c) 2013 Les Jardins d'Avalon (http://www.lesjardinsdavalon.fr)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Description of JardinsAvalon_GiftMessage_Helper_Message
 *
 * @category    JardinsAvalon
 * @package     JardinsAvalon_GiftMessage
 * @author      Pierre-Gildas MILLON <pg.millon@gmail.com>
 */
class JardinsAvalon_GiftMessage_Helper_Message extends Mage_GiftMessage_Helper_Message {

  public function isMessagesAvailable($type, Varien_Object $entity, $store = null) {
    if($type == 'quote') {
      $items = $entity->getAllItems();
      if(is_array($items) && !empty($items)) {
        if($entity instanceof Mage_Sales_Model_Quote) {
          $_type = $entity->getIsMultiShipping() ? 'address_item' : 'item';
        }
        else {
          $_type = 'order_item';
        }

        foreach ($items as $item) {
          if ($item->getParentItem()) {
            continue;
          }
          if ($_type == 'item' && $this->_getDependenceFromStoreConfig($item->getProduct()->getGiftMessageAvailable(), $store)) {
            return true;
          }
        }
        return false;
      }
    } elseif ($type == 'item') {
      return false;
    }

    return parent::isMessagesAvailable($type, $entity, $store);
  }

}

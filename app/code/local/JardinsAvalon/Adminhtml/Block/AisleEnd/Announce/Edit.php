<?php
/**
 * Les Jardins d'Avalon
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@lesjardinsdavalon.fr so we can send you a copy immediately.
 *
 * @category    JardinsAvalon
 * @package     JardinsAvalon_Adminhtml
 * @copyright   Copyright (c) 2013 Les Jardins d'Avalon (http://www.lesjardinsdavalon.fr)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Description of JardinsAvalon_Adminhtml_Block_AisleEnd_Announce_Edit
 *
 * @category    JardinsAvalon
 * @package     JardinsAvalon_Adminhtml
 * @author      Pierre-Gildas MILLON <pg.millon@gmail.com>
 */
class JardinsAvalon_Adminhtml_Block_AisleEnd_Announce_Edit extends Mage_Adminhtml_Block_Widget_Form_Container {

  public function __construct() {
    parent::__construct();

    $this->_objectId   = 'announce_id';
    $this->_blockGroup = 'jardinsavalon_adminhtml';
    $this->_controller = 'aisleEnd_announce';
  }

  /**
   * Retrieve text for header element depending on loaded page
   *
   * @return string
   */
  public function getHeaderText() {
    if(Mage::registry('aisle_end_announce')->getId()) {
      return Mage::helper('aisle_end')->__("Edit Announce '%d'", $this->htmlEscape(Mage::registry('aisle_end_announce')->getId()));
    } else {
      return Mage::helper('aisle_end')->__('New Announce');
    }
  }

}

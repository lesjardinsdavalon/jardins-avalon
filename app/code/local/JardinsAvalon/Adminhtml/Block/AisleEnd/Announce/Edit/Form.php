<?php
/**
 * Les Jardins d'Avalon
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@lesjardinsdavalon.fr so we can send you a copy immediately.
 *
 * @category    JardinsAvalon
 * @package     JardinsAvalon_Adminhtml
 * @copyright   Copyright (c) 2013 Les Jardins d'Avalon (http://www.lesjardinsdavalon.fr)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Description of JardinsAvalon_Adminhtml_Block_AisleEnd_Announce_Edit_Form
 *
 * @category    JardinsAvalon
 * @package     JardinsAvalon_Adminhtml
 * @author      Pierre-Gildas MILLON <pg.millon@gmail.com>
 */
class JardinsAvalon_Adminhtml_Block_AisleEnd_Announce_Edit_Form extends Mage_Adminhtml_Block_Widget_Form {

  const FIELD_IMAGE = 'image_name';
  
  protected $announce;
  
  /**
   *
   * @return JardinsAvalon_Adminhtml_Block_AisleEnd_Announce_Edit_Form
   */
  protected function _prepareForm() {
    $form = new Varien_Data_Form(array(
      'id'      => 'edit_form',
      'action'  => $this->getUrl('*/*/save', array('id' => $this->getRequest()->getParam('id'))),
      'enctype' => 'multipart/form-data',
      'method'  => 'post'));

    $form->setUseContainer(true);
    $this->setForm($form);
    
    $model = $this->getAnnounce();

    $fieldset = $form->addFieldset('announce_fieldset', array(
      'legend' => Mage::helper('aisle_end')->__('Announce'), 
      'class' => 'fieldset-wide'));
    
    $fieldset->addField('target_url', 'text', array(
      'label' => Mage::helper('aisle_end')->__('Target'),
      'class' => 'required-entry',
      'name'  => 'target_url',
      'required' => true,
    ));
    
    $fieldset->addField('image_name', 'announce_image', array(
      'label' => Mage::helper('aisle_end')->__('Image'),
      'name'  => 'image_name',
      'class' => 'required-entry',
      'required' => true,
    ));

    $fieldset->addField('position', 'text', array(
      'label' => Mage::helper('aisle_end')->__('Position'),
      'name'  => 'position',
      'class' => 'required-entry',
      'required' => true,
    ));

    $form->setValues($model->getData());
    $form->addValues(array(
      'image_name' => $model->getImageUrl()
    ));

    return parent::_prepareForm();
  }

  /**
   * 
   * @return JardinsAvalon_AisleEnd_Model_Announce
   */
  public function getAnnounce() {
    if(is_null($this->announce)) {
      $this->announce = Mage::registry('aisle_end_announce');
    }
    return $this->announce;
  }

}
<?php
/**
 * Les Jardins d'Avalon
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@lesjardinsdavalon.fr so we can send you a copy immediately.
 *
 * @category    JardinsAvalon
 * @package     JardinsAvalon_Adminhtml
 * @copyright   Copyright (c) 2013 Les Jardins d'Avalon (http://www.lesjardinsdavalon.fr)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Description of JardinsAvalon_Adminhtml_Block_AisleEnd_Announce_Grid
 *
 * @category    JardinsAvalon
 * @package     JardinsAvalon_Adminhtml
 * @author      Pierre-Gildas MILLON <pg.millon@gmail.com>
 */
class JardinsAvalon_Adminhtml_Block_AisleEnd_Announce_Grid extends Mage_Adminhtml_Block_Widget_Grid {

  protected function _prepareCollection() {
    $collection = Mage::getModel('aisle_end/announce')->getCollection();
    $this->setCollection($collection);
    $this->setDefaultSort('position');
    $this->setDefaultDir('ASC');

    return parent::_prepareCollection();
  }

  protected function _prepareColumns() {

    $this->addColumn('image', array(
      'header' => Mage::helper('aisle_end')->__('Image'),
      'getter' => 'getImageUrl',
      'filter' => false,
      'sortable' => false,
      'renderer' => 'JardinsAvalon_Adminhtml_Block_Widget_Grid_Column_Renderer_Image'
    ));

    $this->addColumn('target', array(
      'header' => Mage::helper('aisle_end')->__('Target'),
      'index' => 'target_url',
      'sortable' => false,
    ));

    $this->addColumn('position', array(
      'header' => Mage::helper('aisle_end')->__('Position'),
      'index' => 'position',
      'type' => 'number',
      'filter' => false,
      'sortable' => true,
    ));

    return parent::_prepareColumns();
  }

  public function getRowUrl($row) {
    return $this->getUrl('*/*/edit', array(
       'id' => $row->getId())
    );
  }

}

<?php
/**
 * Les Jardins d'Avalon
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@lesjardinsdavalon.fr so we can send you a copy immediately.
 *
 * @category    JardinsAvalon
 * @package     JardinsAvalon_Adminhtml
 * @copyright   Copyright (c) 2013 Les Jardins d'Avalon (http://www.lesjardinsdavalon.fr)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Description of JardinsAvalon_Adminhtml_Block_AisleEnd_NewProduct_Edit_Search_Grid
 *
 * @category    JardinsAvalon
 * @package     JardinsAvalon_Adminhtml
 * @author      Pierre-Gildas MILLON <pg.millon@gmail.com>
 *
 * @method int getValue()
 */
class JardinsAvalon_Adminhtml_Block_AisleEnd_NewProduct_Edit_Search_Grid extends Mage_Adminhtml_Block_Widget_Grid {

  public function __construct() {
    parent::__construct();
    $this->setId('bundle_selection_search_grid');
    $this->setRowClickCallback('aisleEndSearchGrid.productGridRowClick.bind(aisleEndSearchGrid)');
    $this->setRowInitCallback('aisleEndSearchGrid.productGridRowInit.bind(aisleEndSearchGrid)');
    $this->setDefaultSort('id');
    $this->setUseAjax(true);
  }

  public function getGridUrl() {
    return $this->getUrl('*/*/grid', array('index' => $this->getIndex()));
  }

  protected function _afterToHtml($html) {
    return parent::_afterToHtml($html);
  }

  protected function _prepareCollection() {
    /** @var $collection Mage_Catalog_Model_Resource_Product_Collection */
    $collection = Mage::getModel('catalog/product')->getCollection()
      ->setStore($this->getStore())
      ->addAttributeToSelect('name')
      ->addAttributeToSelect('sku')
      ->addAttributeToSelect('price')
      ->addStoreFilter();

    if($this->getFirstShow()) {
      $collection->addIdFilter('-1');
      $this->setEmptyText($this->__('Please enter search conditions to view products.'));
    }

    Mage::getSingleton('catalog/product_status')->addSaleableFilterToCollection($collection);

    $this->setCollection($collection);

    return parent::_prepareCollection();
  }

  public function getStore() {
    return Mage::app()->getStore();
  }

  protected function _prepareColumns() {
    $this->addColumn('id', array('header' => Mage::helper('sales')->__('ID'), 'sortable' => true, 'width' => '60px', 'index' => 'entity_id'));
    $this->addColumn('name', array('header' => Mage::helper('sales')->__('Product Name'), 'index' => 'name', 'column_css_class' => 'name'));

    $this->addColumn('sku', array('header' => Mage::helper('sales')->__('SKU'), 'width' => '80px', 'index' => 'sku', 'column_css_class' => 'sku'));
    $this->addColumn('price', array('header' => Mage::helper('sales')->__('Price'), 'align' => 'center', 'type' => 'currency', 'currency_code' => $this->getStore()->getCurrentCurrencyCode(), 'rate' => $this->getStore()->getBaseCurrency()->getRate($this->getStore()->getCurrentCurrencyCode()), 'index' => 'price'));

    $this->addColumn('is_selected', array('header_css_class' => 'a-center', 'type' => 'radio', 'html_name' => 'is_selected', 'align' => 'center', 'index' => 'entity_id', 'filter' => false, 'value' => $this->getValue()));

    return parent::_prepareColumns();
  }
}

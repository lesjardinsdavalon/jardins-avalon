<?php
/**
 * Les Jardins d'Avalon
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@lesjardinsdavalon.fr so we can send you a copy immediately.
 *
 * @category    JardinsAvalon
 * @package     JardinsAvalon_Adminhtml
 * @copyright   Copyright (c) 2013 Les Jardins d'Avalon (http://www.lesjardinsdavalon.fr)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Description of JardinsAvalon_Adminhtml_Block_AisleEnd_NewProduct_Delete_Form
 *
 * @category    JardinsAvalon
 * @package     JardinsAvalon_Adminhtml
 * @author      Pierre-Gildas MILLON <pg.millon@gmail.com>
 */
class JardinsAvalon_Adminhtml_Block_AisleEnd_NewProduct_Delete_Form extends Mage_Adminhtml_Block_Widget_Form {

  /**
   *
   * @return JardinsAvalon_Adminhtml_Block_AisleEnd_NewProduct_Delete_Form
   */
  protected function _prepareForm() {

    $elementId = $this->getElementId();

    $form = new Varien_Data_Form(array(
      'action'  => $this->getUrl('*/*/delete'),
      'enctype' => 'multipart/form-data',
      'method'  => 'post'));

    $form->setUseContainer(true);

    $form->addField(
      'trigger',
      'html',
      array(
        'html' => $this->getLayout()->createBlock('adminhtml/widget_button', '', array(
            'label' => Mage::helper('aisle_end')->__('Delete'),
            'class' => 'delete delete-new-product',
            'type'  => 'submit'
          ))->toHtml()
      ));

    $form->addField(
      'delete_'.$elementId,
      'hidden',
      array(
        'name' => 'id',
        'required' => true,
        'value' => $elementId
      ));

    $this->setForm($form);

    return parent::_prepareForm();
  }

}
<?php
/**
 * Les Jardins d'Avalon
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@lesjardinsdavalon.fr so we can send you a copy immediately.
 *
 * @category    JardinsAvalon
 * @package     JardinsAvalon_Adminhtml
 * @copyright   Copyright (c) 2013 Les Jardins d'Avalon (http://www.lesjardinsdavalon.fr)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Description of JardinsAvalon_Adminhtml_Block_AisleEnd_NewProduct_Grid
 *
 * @category    JardinsAvalon
 * @package     JardinsAvalon_Adminhtml
 * @author      Pierre-Gildas MILLON <pg.millon@gmail.com>
 */
class JardinsAvalon_Adminhtml_Block_AisleEnd_NewProduct_Grid extends Mage_Adminhtml_Block_Widget_Grid {

  public function __construct($attributes = array()) {
    parent::__construct($attributes);

    $this->setDefaultSort('position');
    $this->setDefaultDir('ASC');
  }

  protected function _prepareCollection() {

    $collection = Mage::getModel('aisle_end/newProduct')->getCollection();
    /* @var $collection JardinsAvalon_AisleEnd_Model_Resource_NewProduct_Collection */

    $collection->getSelect()->joinLeft(
        array('catalog_product' => $collection->getTable('catalog/product')),
        'mage_product_id = catalog_product.entity_id'
    );

    $collection->addEavAttributeToSelect(Mage_Catalog_Model_Product::ENTITY, 'name');

    $this->setCollection($collection);

    return parent::_prepareCollection();
  }

  protected function _prepareColumns() {

    $this->addColumn('sku', array(
      'header' => Mage::helper('catalog')->__('SKU'),
      'width' => '80px',
      'index' => 'sku'
    ));

    $this->addColumn('name', array(
      'header' => Mage::helper('catalog')->__('Name'),
      'index' => 'name'
    ));

    $this->addColumn('position', array(
      'header' => Mage::helper('aisle_end')->__('Position'),
      'index' => 'position',
      'type' => 'number',
      'filter' => false,
      'sortable' => true,
    ));

    $this->addColumn('action', array(
      'header'    => Mage::helper('catalog')->__('Action'),
      'width'     => '50px',
      'type'      => 'action',
      'getter'    => 'getProductId',
      'filter'    => false,
      'sortable'  => false,
      'index'     => 'stores',
      'renderer'  => 'JardinsAvalon_Adminhtml_Block_Widget_Grid_Column_Renderer_Post_Action'
    ));

    return parent::_prepareColumns();
  }

  /**
   * @return Mage_Core_Model_Store
   */
  protected function getStore() {
    $storeId = (int)$this->getRequest()->getParam('store', 0);
    return Mage::app()->getStore($storeId);
  }

  public function getRowUrl($row) {
    return $this->getUrl('*/*/edit', array(
            'id' => $row->getId())
    );
  }

}

<?php
/**
 * Les Jardins d'Avalon
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@lesjardinsdavalon.fr so we can send you a copy immediately.
 *
 * @category    JardinsAvalon
 * @package     JardinsAvalon_Adminhtml
 * @copyright   Copyright (c) 2013 Les Jardins d'Avalon (http://www.lesjardinsdavalon.fr)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Description of JardinsAvalon_Adminhtml_AisleEnd_AnnouncementController
 *
 * @category    JardinsAvalon
 * @package     JardinsAvalon_Adminhtml
 * @author      Pierre-Gildas MILLON <pg.millon@gmail.com>
 */
class JardinsAvalon_Adminhtml_AisleEnd_AnnouncementController extends Mage_Adminhtml_Controller_Action {

  protected function getImageFieldName() {
    return JardinsAvalon_Adminhtml_Block_AisleEnd_Announce_Edit_Form::FIELD_IMAGE;
  }

  public function indexAction() {
    $this->loadLayout();
    $this->renderLayout();
  }

  public function newAction() {
    $this->_forward('edit');
  }

  public function editAction() {
    $this->loadLayout();
    $id = (int) $this->getRequest()->getParam('id');
    $model = Mage::getModel('aisle_end/announce');

    if($id) {
      $model->load($id);
    }

    Mage::register('aisle_end_announce', $model);
    $this->renderLayout();
  }

  public function saveAction() {
    $isImageUploaded = false;
    $imageField = $this->getImageFieldName();
    
    if($data = $this->getRequest()->getPost()) {
      $model = Mage::getModel('aisle_end/announce');
      /* @var $model JardinsAvalon_AisleEnd_Model_Announce */

      if(isset($_FILES[$imageField]['name']) && file_exists($_FILES[$imageField]['tmp_name'])) {
        try {
          $filename = $_FILES[$imageField]['name'];

          $uploader = new Varien_File_Uploader($imageField);
          $uploader->setAllowedExtensions(array('jpeg', 'jpg', 'gif', 'png'));
          $uploader->setAllowRenameFiles(false);
          $uploader->setFilesDispersion(false);

          $uploader->save($model->getBaseImagePath(), $filename);
          $data['image_name'] = $filename;
          $isImageUploaded = true;
        } catch(Exception $e) {
          $this->_getSession()->addException($e, Mage::helper('aisle_end')->__('An error occurred while saving the announce image.'));
          $this->_redirect('*/*/');
          return;
        }
      }
      
      if($id = $this->getRequest()->getParam('id')) {
        $model->load($id);
        if($isImageUploaded && $model->hasData('image_name')) {
          unlink($model->getImagePath());
        }
      }

      $model->setData($data);

      try {
        if($id) {
          $model->setId($id);
        }

        $model->save();

        Mage::getSingleton('adminhtml/session')->addSuccess(
         Mage::helper('aisle_end')->__('The announce has been saved.'));
        Mage::getSingleton('adminhtml/session')->setFormData(false);

        $this->_redirect('*/*/');
        return;
      } catch(Mage_Core_Exception $e) {
        $this->_getSession()->addError($e->getMessage());
      } catch(Exception $e) {
        $this->_getSession()->addException($e, Mage::helper('aisle_end')->__('An error occurred while saving the announce.'));
      }

      $this->_getSession()->setFormData($data);
      $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
      return;
    }
    $this->_redirect('*/*/');
  }

  public function deleteAction() {
    if($id = $this->getRequest()->getParam('id')) {
      try {
        $model = $this->loadAnnounce($id);
        $model->delete();
        //delete image
        Mage::getSingleton('adminhtml/session')->addSuccess(
         Mage::helper('aisle_end')->__('The announce has been deleted.'));
        $this->_redirect('*/*/');
        return;
      } catch(Exception $e) {
        Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
        $this->_redirect('*/*/edit', array('id' => $id));
        return;
      }
    }
  }

  protected function loadAnnounce($announceId) {
    return Mage::getModel('aisle_end/announce')->load($announceId);
  }
  
}


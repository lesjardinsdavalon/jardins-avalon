<?php
/**
 * Les Jardins d'Avalon
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@lesjardinsdavalon.fr so we can send you a copy immediately.
 *
 * @category    JardinsAvalon
 * @package     JardinsAvalon_Adminhtml
 * @copyright   Copyright (c) 2013 Les Jardins d'Avalon (http://www.lesjardinsdavalon.fr)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Description of JardinsAvalon_Adminhtml_AisleEnd_Product_NewController
 *
 * @category    JardinsAvalon
 * @package     JardinsAvalon_Adminhtml
 * @author      Pierre-Gildas MILLON <pg.millon@gmail.com>
 */
class JardinsAvalon_Adminhtml_AisleEnd_Product_NewController extends Mage_Adminhtml_Controller_Action {

  public function indexAction() {
    $this->loadLayout();
    $this->renderLayout();
  }

  public function newAction() {
    $this->_forward('edit');
  }

  public function editAction() {
    $this->loadLayout();
    $id = (int) $this->getRequest()->getParam('id');
    $model = Mage::getModel('aisle_end/newProduct');

    if($id) {
      $model->load($id);
    }

    Mage::register('aisle_end_new_product', $model);
    $this->renderLayout();
  }

  public function saveAction() {
    if($data = $this->getRequest()->getPost()) {
      $model = Mage::getModel('aisle_end/newProduct');
      /* @var $model JardinsAvalon_AisleEnd_Model_NewProduct */

      if($id = $this->getRequest()->getParam('id')) {
        $model->load($id);
      }

      $model->setData($data);

      try {
        if($id) {
          $model->setId($id);
        }

        $model->save();

        Mage::getSingleton('adminhtml/session')->addSuccess(
          Mage::helper('aisle_end')->__('The new product has been saved.'));
        Mage::getSingleton('adminhtml/session')->setFormData(false);

        $this->_redirect('*/*/');
        return;
      } catch(Mage_Core_Exception $e) {
        $this->_getSession()->addError($e->getMessage());
      } catch(Exception $e) {
        $this->_getSession()->addException($e, Mage::helper('aisle_end')->__('An error occurred while saving the new product.'));
      }

      $this->_getSession()->setFormData($data);
      $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
      return;
    }

    $this->_redirect('*/*/');
  }

  public function deleteAction() {
    if($id = $this->getRequest()->getPost('id')) {
      try {
        $model = Mage::getModel('aisle_end/newProduct')->load($id);
        $model->delete();
      } catch(Exception $e) {
        Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
      }
      $this->_redirect('*/*/');
      return;
    }
  }

  public function gridAction()
  {
    return $this->getResponse()->setBody(
      $this->getLayout()
        ->createBlock('jardinsavalon_adminhtml/aisleEnd_newProduct_edit_search_grid')
        ->setIndex($this->getRequest()->getParam('index'))
        ->toHtml()
    );
  }

}


<?php
/**
 * Les Jardins d'Avalon
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@lesjardinsdavalon.fr so we can send you a copy immediately.
 *
 * @category    JardinsAvalon
 * @package     JardinsAvalon_Smtp
 * @copyright   Copyright (c) 2013 Les Jardins d'Avalon (http://www.lesjardinsdavalon.fr)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Description of JardinsAvalon_Smtp_Model_Email_Template
 *
 * @category    JardinsAvalon
 * @package     JardinsAvalon_Smtp
 * @author      Pierre-Gildas MILLON <pg.millon@gmail.com>
 */
class JardinsAvalon_Smtp_Model_Email_Template extends Mage_Core_Model_Email_Template {

  const XML_PATH_SENDING_HOST       = 'system/smtp/host';
  const XML_PATH_SENDING_PORT       = 'system/smtp/port';
  const XML_PATH_SENDING_LOGIN      = 'system/smtp/username';
  const XML_PATH_SENDING_PASSWORD   = 'system/smtp/password';
  const XML_PATH_SENDING_SSL        = 'system/smtp/ssl';
  const XML_PATH_SENDING_AUTH_TYPE  = 'system/smtp/auth';
  
  /**
   * Send mail to recipient
   *
   * @param   array|string       $email        E-mail(s)
   * @param   array|string|null  $name         receiver name(s)
   * @param   array              $variables    template variables
   * @return  boolean
   * */
  public function send($email, $name = null, array $variables = array()) {
    if(!$this->isValidForSend()) {
      Mage::logException(new Exception('This letter cannot be sent.')); // translation is intentionally omitted
      return false;
    }

    $emails = array_values((array) $email);
    $names = is_array($name) ? $name : (array) $name;
    $names = array_values($names);
    foreach($emails as $key => $email) {
      if(!isset($names[$key])) {
        $names[$key] = substr($email, 0, strpos($email, '@'));
      }
    }

    $variables['email'] = reset($emails);
    $variables['name'] = reset($names);

    $mail = $this->getMail();

    $setReturnPath = Mage::getStoreConfig(self::XML_PATH_SENDING_SET_RETURN_PATH);
    switch($setReturnPath) {
      case 1:
        $returnPathEmail = $this->getSenderEmail();
        break;
      case 2:
        $returnPathEmail = Mage::getStoreConfig(self::XML_PATH_SENDING_RETURN_PATH_EMAIL);
        break;
      default:
        $returnPathEmail = null;
        break;
    }

    if($returnPathEmail !== null) {
      $mail->setReturnPath($returnPathEmail);
    }
    
    $transport = $this->getTransport($returnPathEmail);
    
    foreach($emails as $key => $email) {
      $mail->addTo($email, '=?utf-8?B?' . base64_encode($names[$key]) . '?=');
    }

    $this->setUseAbsoluteLinks(true);
    $text = $this->getProcessedTemplate($variables, true);

    if($this->isPlain()) {
      $mail->setBodyText($text);
    } else {
      $mail->setBodyHTML($text);
    }

    $mail->setSubject('=?utf-8?B?' . base64_encode($this->getProcessedTemplateSubject($variables)) . '?=');
    $mail->setFrom($this->getSenderEmail(), $this->getSenderName());

    try {
      $mail->send($transport);
      $this->_mail = null;
    } catch(Exception $e) {
      $this->_mail = null;
      Mage::logException($e);
      return false;
    }

    return true;
  }

  protected function getTransport($returnPathEmail) {
    $host     = Mage::getStoreConfig(self::XML_PATH_SENDING_HOST);
    $port     = Mage::getStoreConfig(self::XML_PATH_SENDING_PORT);
    $login    = Mage::getStoreConfig(self::XML_PATH_SENDING_LOGIN);
    $passwd   = Mage::getStoreConfig(self::XML_PATH_SENDING_PASSWORD);
    $ssl      = Mage::getStoreConfig(self::XML_PATH_SENDING_SSL);
    $authType = Mage::getStoreConfig(self::XML_PATH_SENDING_AUTH_TYPE);
    
    $config = array();
    
    if($host === null) {
      if($returnPathEmail !== null) {
        return new Zend_Mail_Transport_Sendmail("-f".$returnPathEmail);
      } else {
        return new Zend_Mail_Transport_Sendmail();
      }
    }
    
    if($port !== null) {
      $config["port"] = $port;
    }
    
    if($login !== null && $passwd !== null) {
      if($authType !== null) {
        $config["auth"] = strtolower($authType);
      } else {
        $config["auth"] = "login";
      }
      $config["username"] = $login;
      $config["password"] = $passwd;
    }
    
    if($ssl) {
      $config["ssl"] = $ssl;
    }
    
    return new Zend_Mail_Transport_Smtp($host, $config);
  }

}

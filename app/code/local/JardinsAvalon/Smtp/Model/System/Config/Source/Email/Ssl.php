<?php
/**
 * Les Jardins d'Avalon
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@lesjardinsdavalon.fr so we can send you a copy immediately.
 *
 * @category    JardinsAvalon
 * @package     JardinsAvalon_Smtp
 * @copyright   Copyright (c) 2013 Les Jardins d'Avalon (http://www.lesjardinsdavalon.fr)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Description of JardinsAvalon_Smtp_Model_System_Config_Source_Email_Ssl
 *
 * @category    JardinsAvalon
 * @package     JardinsAvalon_Smtp
 * @author      Pierre-Gildas MILLON <pg.millon@gmail.com>
 */
class JardinsAvalon_Smtp_Model_System_Config_Source_Email_Ssl {

  public function toOptionArray() {
    return array(
      array('value' => 'NONE', 'label' => 'NONE'),
      array('value' => 'ssl', 'label' => 'SSL'),
      array('value' => 'tls', 'label' => 'TLS'),
    );
  }

}

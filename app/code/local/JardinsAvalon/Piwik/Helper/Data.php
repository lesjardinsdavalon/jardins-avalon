<?php
/**
 * (C) Copyright 2016 Les Jardins d'Avalon
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@lesjardinsdavalon.fr so we can send you a copy immediately.
 */

/**
 * Description of JardinsAvalon_Piwik_Helper_Data
 *
 * @category    JardinsAvalon
 * @package     JardinsAvalon_Piwik
 * @author      Pierre-Gildas MILLON <pgmillon@gmail.com>
 */
class JardinsAvalon_Piwik_Helper_Data extends Mage_Core_Helper_Abstract {
  const XML_PATH_ACTIVE   = 'piwik/analytics/active';
  const XML_PATH_SITE     = 'piwik/analytics/site';
  const XML_PATH_INSTALL  = 'piwik/analytics/install';
  const XML_PATH_TOKEN    = 'piwik/analytics/pwtoken';

  /**
   * @var int[]
   */
  protected $orderIds;

  /**
   * @param mixed $store
   * @return bool
   */
  public function isPiwikAnalyticsAvailable($store = null) {
    return !Mage::helper('core/cookie')->isUserNotAllowSaveCookie()
        && Mage::getStoreConfig(self::XML_PATH_SITE, $store)
        && Mage::getStoreConfigFlag(self::XML_PATH_ACTIVE, $store);
  }

  /**
   * @return int[]
   */
  public function getOrderIds() {
    return $this->orderIds;
  }

  /**
   * @param int[] $orderIds
   */
  public function setOrderIds($orderIds) {
    $this->orderIds = $orderIds;
  }

}
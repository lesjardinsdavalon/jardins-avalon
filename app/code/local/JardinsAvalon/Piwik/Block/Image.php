<?php
/**
 * (C) Copyright 2016 Les Jardins d'Avalon
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@lesjardinsdavalon.fr so we can send you a copy immediately.
 */

/**
 * Description of JardinsAvalon_Piwik_Image_Block
 *
 * @category    JardinsAvalon
 * @package     JardinsAvalon_Catalog
 * @author      Pierre-Gildas MILLON <pg.millon@gmail.com>
 */
class JardinsAvalon_Piwik_Block_Image extends Mage_Core_Block_Template implements Mage_Widget_Block_Interface {

  public function __construct() {
    parent::__construct();
    $this->setTemplate('piwik/image.phtml');
  }

}
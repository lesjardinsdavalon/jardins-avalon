<?php
/**
 * (C) Copyright 2016 Les Jardins d'Avalon
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@lesjardinsdavalon.fr so we can send you a copy immediately.
 */

/**
 * Description of JardinsAvalon_Piwik_Block_Tracker
 *
 * @method null setOrderIds(array $ids)
 * @method array getOrderIds()
 *
 * @category    JardinsAvalon
 * @package     JardinsAvalon_Piwik
 * @author      Pierre-Gildas MILLON <pgmillon@gmail.com>
 */
class JardinsAvalon_Piwik_Block_Tracker extends Mage_Core_Block_Template {

  const HELPER_CLASS = 'jardinsavalon_piwik/data';

  public function __construct(array $args = array()) {
    parent::__construct($args);

    $this->setTemplate('piwik/tracker.phtml');
  }

  protected function _toHtml() {
    if($this->helper(self::HELPER_CLASS)->isPiwikAnalyticsAvailable()) {
      return parent::_toHtml();
    }
    return '';
  }

  /**
   * @return string
   */
  protected function getPiwikCall() {
    return sprintf('_paq.push(%s);' . PHP_EOL, json_encode(func_get_args()));
  }

  /**
   * @param string $name
   * @return mixed
   */
  protected function cleanupProductName($name) {
    return str_replace('"', null, $name);
  }

  /**
   * @param Mage_Catalog_Model_Product $product
   * @return null|string
   */
  protected function getProductFirstCategoryName($product) {
    $categories = $product->getCategoryIds();

    /** @var Mage_Catalog_Model_Category $category */
    $category = isset($categories[0]) ? Mage::getModel('catalog/category')->load($categories[0]) : null;
    return $category ? $category->getName() : null;
  }

  public function getCartUpdate() {
    /** @var Mage_Sales_Model_Quote $quote */
    $quote = Mage::getModel('checkout/cart')->getQuote();
    $out = '';

    /** @var Mage_Sales_Model_Quote_Item $item */
    foreach($quote->getAllVisibleItems() as $item) {
      /** @var Mage_Catalog_Model_Product $product */
      $product = Mage::getModel('catalog/product')->load($item->product_id);

      if(0.00001 < $item->getPrice()) {
        $out .= $this->getPiwikCall(
          'addEcommerceItem',
          $item->getSku(),
          $this->cleanupProductName($item->getName()),
          $this->getProductFirstCategoryName($product),
          $item->getPrice(),
          $item->getQty()
        );
      }
    }

    if(0 < $quote->getGrandTotal()) {
      $out .= $this->getPiwikCall(
        'trackEcommerceCartUpdate',
        $quote->getGrandTotal()
      );
    }

    return $out;
  }

  public function getProductView() {
    /** @var Mage_Catalog_Model_Product $product */
    $product = Mage::registry('current_product');

    if($product instanceof Mage_Catalog_Model_Product) {
      return $this->getPiwikCall(
        'setEcommerceView',
        $product->getSku(),
        $this->cleanupProductName($product->getName()),
        $this->getProductFirstCategoryName($product),
        $product->getPrice()
      );
    }

    return '';
  }

  public function getCategoryView() {
    /** @var Mage_Catalog_Model_Category $category */
    $category = Mage::registry('current_category');

    if($category instanceof Mage_Catalog_Model_Category) {
      return $this->getPiwikCall(
        'setEcommerceView',
        false,
        false,
        $category->getName()
      );
    }

    return '';
  }

  public function getOrderView() {
    $order_ids = $this->helper(self::HELPER_CLASS)->getOrderIds();
    $out = '';

    if(!empty($order_ids)) {
      foreach($order_ids as $order_id) {
        /** @var Mage_Sales_Model_Order $order */
        $order = Mage::getModel('sales/order')->load($order_id);

        /** @var Mage_Sales_Model_Order_Item $item */
        foreach($order->getAllVisibleItems() as $item) {
          /** @var Mage_Catalog_Model_Product $product */
          $product = Mage::getModel('catalog/product')->load($item->getProductId());

          if(0.00001 < $item->getPrice()) {
            $out .= $this->getPiwikCall(
              'addEcommerceItem',
              $item->getSku(),
              $this->cleanupProductName($item->getName()),
              $this->getProductFirstCategoryName($product),
              $item->getPrice(),
              $item->getQtyOrdered() ? number_format($item->getQtyOrdered(), 0, '.', '') : 0
            );
          }
        }

        $out .= $this->getPiwikCall(
          'trackEcommerceOrder',
          $order->getIncrementId(),
          $order->getBaseGrandTotal(),
          $order->getGrandTotal() ? $order->getGrandTotal() - $order->getShippingAmount() - $order->getShippingTaxAmount() : 0,
          $order->getBaseTaxAmount(),
          $order->getBaseShippingAmount()
        );
      }

    }

    return $out;
  }

}
<?php
/**
 * Les Jardins d'Avalon
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@lesjardinsdavalon.fr so we can send you a copy immediately.
 *
 * @category    JardinsAvalon
 * @package     JardinsAvalon_Catalog
 * @copyright   Copyright (c) 2013 Les Jardins d'Avalon (http://www.lesjardinsdavalon.fr)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * JardinsAvalon_Rss_Block_Catalog_Review
 *
 * @category    JardinsAvalon
 * @package     JardinsAvalon_Rss
 * @author      Pierre-Gildas MILLON <pg.millon@gmail.com>
 */
class JardinsAvalon_Rss_Block_Catalog_Review extends Mage_Rss_Block_Catalog_Review {

  /**
   * Format single RSS element
   *
   * @param array $args
   * @return null
   */
  public function addReviewItemXmlCallback($args) {
    $rssObj = $args['rssObj'];
    $row = $args['row'];

    $store = Mage::app()->getStore($row['store_id']);
    $urlModel = Mage::getModel('core/url')->setStore($store);
    $productUrl = $urlModel->getUrl('catalog/product/view', array('id' => $row['entity_id']));
    $reviewUrl = Mage::helper('adminhtml')->getUrl(
      'adminhtml/catalog_product_review/edit/',
      array('id' => $row['review_id'], '_secure' => true, '_nosecret' => true));
    $storeName = $store->getName();
    $pudDate = strtotime($row['review_created_at']);

    $description = '<p>'
      . $this->__('Product: <a href="%s">%s</a> <br/>', $productUrl, $row['name'])
      . $this->__('Summary of review: %s <br/>', $row['title'])
      . $this->__('Review: %s <br/>', $row['detail'])
      . $this->__('Store: %s <br/>', $storeName)
      . $this->__('click <a href="%s">here</a> to view the review', $reviewUrl)
      . '</p>';
    $data = array(
      'title' => $this->__('Product: "%s" review By: %s', $row['name'], $row['nickname']),
      'link' => $reviewUrl,
      'description' => $description,
      'lastUpdate' => $pudDate
    );
    $rssObj->_addEntry($data);
  }
}

<?php
/**
 * (C) Copyright 2016 Les Jardins d'Avalon
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@lesjardinsdavalon.fr so we can send you a copy immediately.
 */

require_once Mage::getModuleDir('controllers','Mage_Contacts').DS.'IndexController.php';

class JardinsAvalon_Contacts_IndexController extends Mage_Contacts_IndexController {

  public function postAction() {

    /** @var Mage_Core_Model_Session $session */
    $session = Mage::getSingleton('core/session');

    /** @var \Monolog\Logger $logger */
    $logger = Mage::helper('jardinsavalon_core/logging')->getLogger();

    /** @var JardinsAvalon_Core_Helper_Captcha $captcha */
    $captcha = Mage::helper('jardinsavalon_core/captcha');

    $logger->info('New contact request', Mage::helper('jardinsavalon_core/logging')->getContextData());

    if(!$this->isFormKeyValid() || !$captcha->isCaptchaValid()) {
      $e = new Mage_Core_Exception($this->__('Invalid form, please try submitting form again.'));
      $session->addException($e, $this->__('There was a problem with the subscription: %s', $e->getMessage()));

      $this->_redirectReferer();

      return;
    }

    parent::postAction();
  }

  protected function isFormKeyValid() {
    /** @var Mage_Core_Model_Session $session */
    $session = Mage::getSingleton('core/session');

    return $session->validateFormKey($this->getRequest()->getPost(Mage_Core_Model_Url::FORM_KEY));
  }

}


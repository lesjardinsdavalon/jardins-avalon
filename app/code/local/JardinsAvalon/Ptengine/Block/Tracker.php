<?php
/**
 *
 * Piwik Extension for Magento created by Adrian Speyer
 * Get Piwik at http://www.piwik.org - Open source web analytics
 *
 * PiwikAnalytics Page Block
 *
 * @category   PiwikMage
 * @package    PiwikMage_PiwikAnalytics
 * @copyright   Copyright (c) 2012 Adrian Speyer. (http://www.adrianspeyer.com)
 * @license     @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 *
 */

class JardinsAvalon_Ptengine_Block_Tracker extends Mage_Core_Block_Template
{

    /**
     * Render Piwik tracking scripts
     *
     * @return string
     */
    protected function _toHtml()
    {
        if (!Mage::helper('ptengine')->isPtengineEnabled()) {
            return '';
        }

        return parent::_toHtml();
    }
}

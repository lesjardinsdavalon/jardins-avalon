<?php

/**
 *
 * @author Pierre-Gildas MILLON <pgmillon@gmail.com>
 */
class JardinsAvalon_Ptengine_Helper_Data extends Mage_Core_Helper_Abstract {
  const XML_PATH_PTENGINE_ACTIVE = 'ptengine/analytics/active';
  const XML_PATH_PTENGINE_ACCOUNTID = 'ptengine/analytics/accountid';

  /**
   *
   * @param mixed $store
   * @return bool
   */
  public function isPtengineEnabled($store = null)
  {
    $accountId = Mage::getStoreConfig(self::XML_PATH_PTENGINE_ACCOUNTID, $store);
    return $accountId && Mage::getStoreConfigFlag(self::XML_PATH_PTENGINE_ACTIVE, $store);
  }
}
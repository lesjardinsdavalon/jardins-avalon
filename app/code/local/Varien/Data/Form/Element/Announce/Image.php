<?php
/**
 * Les Jardins d'Avalon
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@lesjardinsdavalon.fr so we can send you a copy immediately.
 *
 * @category    Varien
 * @package     Varien_Data
 * @copyright   Copyright (c) 2013 Les Jardins d'Avalon (http://www.lesjardinsdavalon.fr)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Description of Varien_Data_Form_Element_Announce_Image
 *
 * @category    Varien
 * @package     Varien_Data
 * @author      Pierre-Gildas MILLON <pg.millon@gmail.com>
 */
class Varien_Data_Form_Element_Announce_Image extends Varien_Data_Form_Element_Image {

  protected function _getDeleteCheckbox() {
    return null;
  }
  
}
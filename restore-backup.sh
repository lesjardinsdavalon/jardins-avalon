#!/bin/bash -x

if [ -z "$1" ]; then
  echo "Usage: restore-backup.sh file"
  exit 1
fi

ROOT_PASSWD="7RQi?s7wVy7j=UfK"

kubectl exec -it deploy/mysql -- mysql -u root -p${ROOT_PASSWD} -e "DROP DATABASE ljda; CREATE SCHEMA ljda DEFAULT CHARACTER SET utf8 COLLATE utf8_bin;"

set -e

kubectl exec -n ljda-dev -it deploy/mysql -- mysql -u root -p${ROOT_PASSWD} ljda < "$1"

cat <<EOF | kubectl exec -n ljda-dev -it --container php-fpm deploy/ljda-main -- php
<?php
  chdir(__DIR__);
  require 'app/Mage.php';

  \$cacheOptions = array();
  foreach(Mage::app()->getCacheInstance()->getTypes() as \$key => \$value) {
    \$cacheOptions[\$key] = 0;
  }
  Mage::app()->saveUseCache(\$cacheOptions);

  \$config = Mage::getModel('core/config');

  \$config->saveConfig('web/unsecure/base_url', 'http://ljda.minikube.lan/');
  \$config->saveConfig('web/secure/base_url', 'http://ljda.minikube.lan/');
  \$config->saveConfig('smtppro/general/option', 'smtp');
  \$config->saveConfig('smtppro/general/smtp_authentication', 'none');
  \$config->saveConfig('smtppro/general/smtp_host', 'fakesmtp');
  \$config->saveConfig('smtppro/general/smtp_port', '25');
#  \$config->saveConfig('paypal/general/business_account', 'pg.millon-facilitator@gmail.com');
#  \$config->saveConfig('paypal/wpp/api_username', Mage::helper('core')->encrypt('pg.millon-facilitator_api1.gmail.com'));
#  \$config->saveConfig('paypal/wpp/api_password', Mage::helper('core')->encrypt('1362933471'));
#  \$config->saveConfig('paypal/wpp/api_signature', Mage::helper('core')->encrypt('AQhuwxIzqfUQp64-3IeuZXw8uqvkAs1mpRIMjkSa-U0uXZYGHgS2.Jgv'));
#  \$config->saveConfig('paypal/wpp/api_authentication', 0);
EOF

# Code

## Requirements

 * PHP >= 5.3.23
 * Docker (for testing & releasing)
 * Python (for releasing)

## Getting started

```
$ git clone git@bitbucket.org:lesjardinsdavalon/jardins-avalon.git
$ ./restore-backup.sh BACKUP_FILE.sql
$ docker-compose up
```

You have a running website at http://localhost:8000/

## Releasing

```
$ pip install release-requirements.txt
$ python2 release.py
```

## Packaging

```
$ ./rpmbuild
```

# License

[Open Software License ("OSL"), Version 3.0](/LICENSE.html)

# Open-Source Licenses generously granted

[PhpStorm](https://www.jetbrains.com/phpstorm/)

[JIRA](https://www.atlassian.com/software/jira)

[AppDynamics](https://www.appdynamics.com/)

[BrowserStack](https://www.browserstack.com/)

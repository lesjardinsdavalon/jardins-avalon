K3D_CLUSTER=ljda_magento

# If the first argument is "restore"...
ifeq (restore,$(firstword $(MAKECMDGOALS)))
  # use the rest as arguments for "restore"
  RUN_ARGS := $(wordlist 2,$(words $(MAKECMDGOALS)),$(MAKECMDGOALS))
  # ...and turn them into do-nothing targets
  $(eval $(RUN_ARGS):;@:)
endif

.PHONY: k3d-cluster deploy/dev restore

k3d:
	k3d cluster delete $(K3D_CLUSTER) || true
	k3d cluster create --config=deploy/dev/k3d-config.yaml
	k3d kubeconfig get ljda-magento > deploy/dev/kubeconfig.yml

deploy/dev:
	pushd deploy/dev; \
	terraform apply --

restore:
	export KUBECONFIG=deploy/dev/kubeconfig.yml; \
	./restore-backup.sh $(RUN_ARGS)
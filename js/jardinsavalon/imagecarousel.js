/**
 * Les Jardins d'Avalon
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@lesjardinsdavalon.fr so we can send you a copy immediately.
 *
 * @category    JardinsAvalon
 * @package     Javascripts
 * @copyright   Copyright (c) 2013 Les Jardins d'Avalon (http://www.lesjardinsdavalon.fr)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

var ImageCarousel = new Class.create();

ImageCarousel.prototype = {
  
  initialize: function(args) {
    this.container        = $$(args.container ? args.container : '.image-carousel-container').first();
    this.display          = this.container.select('.display').first();
    this.spinner          = this.container.select('.spinner').first();
    this.activeImage      = args.activeImage ? args.activeImage : '/images/image-carousel-active.png';
    this.inactiveImage    = args.inactiveImage ? args.inactiveImage : '/images/image-carousel-inactive.png';
    this.delay            = args.delay ? args.delay : 5000;
    
    this.timer = false;
    this.lock = false;
    this.current = 0;
    this.queue = [];
    
    this.items = this.container.select('.item');
    this.items.each(function(item, index) {
      if(index !== 0) {
        item.remove();
      }
    });
    
    this.container.select('[data-carousel|=action]').each((function(element) {
      Event.observe(element, 'click', (function(event) {
        event.stop();
        
        var element = event.findElement('a');
        var action = /action-([\w\d]+)/.exec(element.readAttribute('data-carousel'))[1];
        
        this.clearTimer();
        this.queue.push(action);
        this.processQueue();
      }).bind(this));
    }).bind(this));
  },
  
  start: function() {
    if(this.items.size() > 0) {
      this.processQueue();
    }
  },
  
  stepForward: function(callback) {
    var index = this.current + 1;
    if(index === this.items.size()) {
      index = 0;
    }
    this.showItem(index, callback);
  },
  
  stepBackward: function(callback) {
    var index = this.current - 1;
    if(index < 0) {
      index = this.items.size() - 1;
    }
    this.showItem(index, callback);
  },
          
  clearTimer: function() {
    clearTimeout(this.timer);
  },
          
  setupTimer: function() {
    this.timer = setTimeout((function() {
      this.queue.push('forward');
      this.processQueue();
    }).bind(this), this.delay);
  },
  
  processQueue: function() {
    if(this.lock) {
      return;
    }
    
    var action = this.queue.pop();
    if(typeof action !== 'undefined') {
      if(action %1 === 0) {
        this.showItem(parseInt(action, 10), this.processQueue.bind(this));
      } else {
        switch(action) {
          case "forward":
            this.stepForward(this.processQueue.bind(this));
            break;
          case "backward":
            this.stepBackward(this.processQueue.bind(this));
            break;
        }
      }
    } else {
      this.setupTimer();
    }
  },
          
  showItem: function(index, callback) {
    var item = this.items[index];
    var currentItem = this.items[this.current];
    this.clearTimer();
    
    if(index === this.current) {
      return;
    }
    
    this.lock = true;
    this.spinner.insert(currentItem, 'top');
    this.spinner.show();
    this.display.insert(item, 'bottom');
    this.container.select('.viewer-position').each((function(element, viewerIndex) {
      if(index === viewerIndex) {
        element.writeAttribute('src', this.activeImage);
      } else {
        element.writeAttribute('src', this.inactiveImage);
      }
    }).bind(this));

    new Effect.Fade(this.spinner, {
      afterFinish: (function() {
        this.display.insert(item);
        this.spinner.hide();
        currentItem.remove();
        this.current = index;
        this.lock = false;
        if(callback) {
          callback();
        }
      }).bind(this)
    });
  }
  
}